<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Questionnaire Controller
 *
 */
class QuestionnaireController extends AppController {
    /**
     * @desc Function has to be called before excution of every funtion
     * @param nill
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        if ($this->Auth->user()) {
            $userDetail = $this->Auth->user();
            $userName = $userDetail['username'];
            $this->set('userName', $userName);
        }
    }

    /**
     * @desc Function responsible to get questionnaire list from limesurvey and assign it to view
     * @param nill
     * @author Suman Khatri on 5th October 2015
     */
    public function questionnairelist() {
        $this->loadComponent('Questionnaire');
        $this->set('title', "Questionnaire | List | Admin");
        $questionnaireList = $this->Questionnaire->getQuestionnaireList();
        $this->set('questionnaireList', $questionnaireList);
        $this->layout = 'loggedinlayout';
    }
    
    /**
     * @desc Function responsible to get questionnaire detail from limesurvey
     * @param qId integer id of questionnaire whose details has to be fetched
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getquestionnairedetails() {
        $this->autoRender = false;
        $qCode = array();
        $qId = $this->request->data['qid'];
        $this->loadComponent('Questionnaire');
        $questionList = $this->Questionnaire->getQuestionnaireDetails($qId);
        if(!empty($questionList)) {
            if(isset($questionList['status']) && !empty($questionList['status'])) {
                $response = array('status' => 0, 'codeList' => $questionList['status']);
            } else {
                foreach ($questionList as $qList) {
                    $qQuestionCodes = explode("{", $qList['question']);
                    if(!empty($qQuestionCodes) && strpos($qList['question'], '}') !== false) {
                        for ($i=1; $i < count($qQuestionCodes); $i = $i+2) {
                            $result = mb_substr($qQuestionCodes[$i], 0, 2);
                                if(strtolower($result) != 'if') {
                                $code = explode("}", $qQuestionCodes[$i]);
                                $qCode[] = $code[0];
                            }
                        }
                    }
                    $qCode[] =  $qList['title'];
                }
                $response = array('status' => 1, 'codeList' => array_unique($qCode));
            }
        } else {
            $response = array('status' => 0, 'codeList' => null);
        }
        echo json_encode($response);die;
    }
    
}