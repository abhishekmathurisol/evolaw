<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Michelf\Markdown;
use Cake\Core\Exception\Exception;

require_once(ROOT . '/vendor'.DS.'smarty'.DS.'Smarty.class.php');
use \Smarty;
/**
 * Document Controller
 *
 */
class DocumentController extends AppController {
    /**
     * @desc Function has to be called before excution of every funtion
     * @param nill
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        if ($this->Auth->user()) {
            $userDetail = $this->Auth->user();
            $userName = $userDetail['username'];
            $this->set('userName', $userName);
        }
    }

    /**
     * @desc Function responsible for add and to get document details for edit
     * @param $qId integer id of document whose details has to be fetched
     * @return arrayIterator
     * @author Suman Khatri on 6th October 2015
     */
    public function document() {
        $this->set('stateList', $this->getStateList());
        $this->set('questionnaireList', $this->getQuestionnaireList());
        $this->set('documentPartTypeList', $this->getDocumentPartsType());
        $this->set('workflowTemplatesList', $this->getWorkFlowTemplates());
        $this->set('categoryList', $this->getCategoryList());
        if(isset($this->request->query['id']) && !empty($this->request->query['id'])){
            //loading modal Documents
            $documentTable = TableRegistry::get('Documents');
            //getting document details
            $documentDetail = $documentTable->find('all',array(
                'conditions' => array(
                    'id' => $this->request->query['id']
                )
            ))->toArray();
            //loading modal DocumentPart
            $documentParts = TableRegistry::get('DocumentParts');
            //getting document parts details
            $documentPartsDetail = $documentParts->find('all',array(
                'fields' => array('DocumentParts.id','DocumentParts.workflow_template_id',
                    'DocumentParts.document_part_type_id','DocumentParts.name','DocumentParts.document_id',
                    'DocumentParts.sort_order', 'DocumentParts.conditions','workfolwTemplate.name','DocumentPartTypes.name'),
                'conditions' => array(
                    'document_id' => $this->request->query['id']
                ),
            ))->join([
                'workfolwTemplate' => [
                'table' => 'workflow_templates',
                'type' => 'LEFT',
                'conditions' => 'workfolwTemplate.id = DocumentParts.workflow_template_id',
                ],
                'DocumentPartTypes' => [
                'table' => 'document_part_types',
                'type' => 'LEFT',
                'conditions' => 'DocumentPartTypes.id = DocumentParts.document_part_type_id',
                ]
            ])->order("DocumentParts.sort_order ASC")->toArray();
            //echo "<pre>";print_r($documentPartsDetail);die;
            $this->set('document', $documentDetail);
            $this->set('documentparts', $documentPartsDetail);
            $this->set('title', "EvoLaw | Update Document | Admin");
        } else {
            $this->set('title', "EvoLaw | Add Document | Admin");
        }
        $docId = isset($this->request->query['id'])?$this->request->query['id']:'';
        $this->set('docId',$docId);
        $this->layout = 'loggedinlayout';
    }

    /**
     * @desc Function responsible to get state list
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getStateList() {
        //getting states from model table to show in view
        $statesTable = TableRegistry::get('States');
        $stateList = $statesTable->find("list")->toArray();
        return $stateList;
    }

    /**
     * @desc Function responsible to get questionnaire list
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getQuestionnaireList() {
        //getting questionnaire list from component to show in view
        $this->loadComponent('Questionnaire');
        $questionnaireList = $this->Questionnaire->getquestionnairelist();
        $questionnaireList = $this->Questionnaire->formatQuestionnaireList($questionnaireList);
        return $questionnaireList;
    }

    /**
     * @desc Function responsible to get types of document part
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getDocumentPartsType() {
        //getting document part types from model table to show in view
        $documentPartTypes = TableRegistry::get('DocumentPartTypes');
        $documentPartTypeList = $documentPartTypes->find("list")->toArray();
        return $documentPartTypeList;
    }

    /**
     * @desc Function responsible to get workflow templates
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getWorkFlowTemplates() {
        //getting workflow templates from model table to show in view
        $workflowTemplates = TableRegistry::get('WorkflowTemplates');
        $workflowTemplatesList = $workflowTemplates->find("list")->toArray();
        return $workflowTemplatesList;
    }

    /**
     * @desc Function responsible to get category list
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 12th October 2015
     */
    public function getCategoryList() {
        //getting category from model table to show in view
        $categoryTable = TableRegistry::get('category');
        $categoryList = $categoryTable->find("list", array(
            'fields' => array('id','name'),
            'conditions' => array('parent_id !=' => 0)
        ))->toArray();
        return $categoryList;
    }

    /**
     * @desc Function responsible to save documents and document parts
     * @param dara array array of fields of document form
     * @return arrayObject
     * @author Suman Khatri on 13th October 2015
     */
    public function save() {
        $this->autoRender = false;
        parse_str($this->request->data['data'], $dataArray);//print_r($dataArray);die;
        $saveDoc = array();
        if(($this->request->data['requestfrom'] == 'saveasdraft') || ($this->request->data['requestfrom'] == 'savepart' && empty($this->request->data['docid']))) {
            $saveDoc = $this->saveDcoument($dataArray);
            if (!$saveDoc) {
                $response = array(
                                'status' => 0,
                                'errorType' => 'doc',
                                'error' => 'An error occurs while saving document'
                            );
            } else {
                $response['status'] = 1;
                $response['docId'] = $saveDoc['id'];
                $response['docDetail'] = $saveDoc;
            }
        }
        if(($this->request->data['requestfrom'] == 'saveasdraft' && empty($dataArray['docid']) ||
            $this->request->data['requestfrom'] == 'savepart')) {
            if(empty($dataArray['docid']) || $dataArray['docid'] == 'undefined') {
                $docId = $saveDoc['id'];
            } else {
                $docId = $dataArray['docid'];
            }
            $saveDocPart = $this->saveDocumentPart($dataArray, $docId);
            if($saveDocPart) {
                if($dataArray['documentparttype'] != 3) {
                    $saveWorlFlow = $this->saveWorkflow($dataArray, $saveDocPart['id'], $docId);
                    if($saveWorlFlow) {
                        $response['status'] = 1;
                        $response['docId'] = $docId;
                        $response['docPartId'] = $saveDocPart['id'];
                        $response['docPartDetail'] = $saveDocPart;
                        $response['workFlows'] = $saveWorlFlow;
                    } else {
                        $response = array(
                            'status' => 0,
                            'errorType' => 'docPartWorkFlow',
                            'error' => 'An error occurs while saving document part\'s workflow'
                        );
                    }
                } else {
                    $response['status'] = 1;
                    $response['docId'] = $docId;
                    $response['docPartId'] = $saveDocPart['id'];
                    $response['docPartDetail'] = $saveDocPart;
                }
            } else {
                $response = array(
                    'status' => 0,
                    'errorType' => 'docPart',
                    'error' => 'An error occurs while saving document part'
                );
            }
        }
       echo json_encode($response);die;
    }

    /**
     * @desc Function responsible to save document details
     * @param $dataString array array of fields
     * @return arrayObject
     * @author Suman Khatri on 14th October 2015
     */
    public function saveDcoument($dataArray) {
        //print_r($dataArray);
        //loading modal Documents
        $documents = TableRegistry::get('Documents');
        $data['Document']['version'] = $dataArray['version'][0].'.'.$dataArray['version'][1].'.'.$dataArray['version'][2];
        $data['Document']['name'] = $dataArray['documenttitle'];
        $data['Document']['state'] = $dataArray['state'];
        $data['Document']['category_id'] = implode($dataArray['documentcategory'], ",");
        $data['Document']['description'] = $dataArray['documentdescription'];
        $data['Document']['associated_questionnaire_id'] = $dataArray['questionnaire'];
        $data['Document']['s_price'] = $dataArray['price'];
        $data['Document']['sku'] = $dataArray['sku'];
        $data['Document']['status'] = $dataArray['status'];
        if(isset($dataArray['docid']) && !empty($dataArray['docid']) && $dataArray['docid'] != 'undefined') {
            $doc = $documents->get($dataArray['docid'], [
                'contain' => []
            ]);
        } else {
            $doc = $documents->newEntity();
        }
        $doc = $documents->patchEntity($doc, $data['Document']);
        $saveDoc = $documents->save($doc);
        return $saveDoc;
    }

    /**
     * @desc Function responsible to save document part details
     * @param $dataString array array of fields
     * @param $docId      int    id of document
     * @return arrayObject
     * @author Suman Khatri on 14th October 2015
     */
    public function saveDocumentPart($dataArray, $docId) {
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        $data['DocumentParts']['document_id'] = $docId;
        $data['DocumentParts']['name'] = $dataArray['documentpartdesc'];
        $data['DocumentParts']['sort_order'] = $dataArray['sortorder'];
        
        $data['DocumentParts']['document_part_type_id'] = $dataArray['documentparttype'];
        if($dataArray['documentparttype'] == 3) {
            $data['DocumentParts']['static_pdf_path'] = $dataArray['pdfname'];
            $template = 0;
        } else {
            $template = $dataArray['template'];
        }
        $data['DocumentParts']['workflow_template_id'] = $template;
        if((isset($dataArray['condition']) && !empty($dataArray['condition']))) {
            if(strtolower($dataArray['condition']) != 'always') {
                $data['DocumentParts']['conditions'] = $dataArray['condition'];
            } else {
                $data['DocumentParts']['conditions'] = $dataArray['condition'];
            }
        }
        if(isset($dataArray['docpartid']) && !empty($dataArray['docpartid'])) {
            $docPart = $documentParts->get($dataArray['docpartid'], [
                'contain' => []
            ]);
        } else {
            $docPart = $documentParts->newEntity();
        }
        $docPart = $documentParts->patchEntity($docPart, $data['DocumentParts']);
        $saveDocPart = $documentParts->save($docPart);
        return $saveDocPart;
    }

    /**
     * @desc Function responsible to save workflow details
     * @param $dataString array array of fields
     * @param $docId      int    id of document
     * @return arrayIterator
     * @author Suman Khatri
     * $created on 14th October 2015
     */
    public function saveWorkflow($dataArray, $docPartId, $docId) {
        //loading modal Workflow
        $workFlowTable = TableRegistry::get('Workflow');
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        $workFlowArray = array();
        if(isset($dataArray['docpartid']) && !empty($dataArray['docpartid'])) {
            $this->deleteDocPartDetails($dataArray, $docPartId);
        }
        if($dataArray['documentparttype'] != 3) {
            $fileName = (1000+$docId).'.'.$dataArray['version'][0].'.'.$dataArray['version'][1].'.'.$dataArray['version'][2].'.'.$docPartId.'.tpl';
            $tplFile = fopen(TPL_PATH.$fileName, 'w');
            $txt = "<html>\n<head>\n<link rel='stylesheet' href='".DOCUMENT_TEMPLATE."workflow_template_id_".$dataArray['template'].".css' type='text/css'>\n</head>\n<body>\n\n";
            fwrite($tplFile, $txt);
            for($i=1;$i<=$dataArray['numofrows'];$i++) {
                $txt = "";
                $data['Workflow']['document_part_id'] = $docPartId;
                $data['Workflow']['section'] = $dataArray["typeofworkflow$i"];
                if(strtolower($dataArray["workflowcondition$i"]) != 'always') {
                    $txt = "{if ".$dataArray["workflowcondition$i"]."}";
                    $data['Workflow']['conditions'] = $dataArray["workflowcondition$i"];
                } else {
                    $txt = "{if 1}";
                    $data['Workflow']['conditions'] = 1;
                }
                fwrite($tplFile, $txt);
                if(!empty($dataArray["workflowmarkdown$i"])) {
					/* Mike on 20151101
					 * Section "type" should insert HTML / Markdown as needed
					 */
					$markdown = "";
					if ($data['Workflow']['section'] == "Title") {
						$markdown = "# " . $dataArray["workflowmarkdown$i"];
					} else if ($data['Workflow']['section'] == "Heading") {
						$markdown = "## " . $dataArray["workflowmarkdown$i"];
					} else if ($data['Workflow']['section'] == "Sub Heading") {
						$markdown = "### " . $dataArray["workflowmarkdown$i"];
					} else if ($data['Workflow']['section'] == "Sub Sub Heading") {
						$markdown = "#### " . $dataArray["workflowmarkdown$i"];
					} else if ($data['Workflow']['section'] == "Paragraph") {
						$markdown = "\n\n" . $dataArray["workflowmarkdown$i"] . "\n\n";
					} else {
						$markdown = $dataArray["workflowmarkdown$i"];
					}                            
					$markdown = str_replace("_", "XUNDERSCOREX", $markdown); // I want to preserve "_" which would normally be considered a markdown operator
                    //$htmlContent = Markdown::defaultTransform($markdown);
                    $htmlContent = $markdown;
					$htmlContent = str_replace("XUNDERSCOREX", "_", $htmlContent); // Put "_" back where it belongs
					 /* END 20151101 */     
                    $data['Workflow']['content'] = $dataArray["workflowmarkdown$i"];
                    //writing text into txt file
                    $txt = $htmlContent."\n{/if}\n";
                    fwrite($tplFile, $txt);
                }
                $workFlow = $workFlowTable->newEntity();
                $workFlow = $workFlowTable->patchEntity($workFlow, $data['Workflow']);
                $workFlowArray[] = $workFlowTable->save($workFlow);
            }
            $txt = "</body>\n</html>";
            fwrite($tplFile, $txt);
            fclose($tplFile);
            $docPart = $documentParts->get($docPartId, [
                'contain' => []
            ]);
            $data['DocumentParts']['document_parts_file_name'] = $fileName;
            $docPart = $documentParts->patchEntity($docPart, $data['DocumentParts']);
            $saveDocPart = $documentParts->save($docPart);
        } 
        return $workFlowArray;
    }
    /**
     * @desc Function responsible to delete workflow details and associated tpl and pdf files
     * @param id integer id of docuemtn part whose workflow details and associated tpl and pdf files has to be deleted
     * @return true/false
     * @author Suman Khatri on 20th October 2015
     */
    public function deleteDocPartDetails($dataArray, $docPartId) {
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        //loading modal Workflow
        $workFlowTable = TableRegistry::get('Workflow');
        $docPartDetails = $documentParts->find('all', [
            'conditions' => ['DocumentParts.id' => $docPartId]
        ])->toArray();
        if($dataArray['documentparttype'] != 3) {
            if(!empty($docPartDetails[0]['document_parts_file_name']) && file_exists(TPL_PATH.$docPartDetails[0]['document_parts_file_name'])) {
                @unlink(TPL_PATH.$docPartDetails[0]['document_parts_file_name']);
            }
            $res = $workFlowTable->deleteAll("document_part_id = $docPartId");
        }
        if(!empty($docPartDetails[0]['static_pdf_path']) && file_exists(STATIC_PDF_PATH.$docPartDetails[0]['static_pdf_path'])&&
                $docPartDetails[0]['static_pdf_path'] != $dataArray['pdfname'] && $dataArray['documentparttype'] == 3) {
            @unlink(STATIC_PDF_PATH.$docPartDetails[0]['static_pdf_path']);
        }
        
    }

     /**
     * @desc Function responsible to get workflow details of document part and workflows
     * @param id integer id of docuemtn part whose details has to be fetched
     * @return arrayIterator
     * @author Suman Khatri on 15th October 2015
     */
    public function getdocpartworkflow() {
        $this->autoRender = false;
        $workFlow = array();
        $docPartId = $this->request->data['id'];
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        $docPartDetails = $documentParts->find('all',array(
            'conditions' => array(
                'DocumentParts.id' => $docPartId
            )
        ))->toArray();
        if(!empty($docPartDetails)) {
            //loading modal DocumentPartWorkFlow
            $workFlowTable = TableRegistry::get('Workflow');
            $workFlowList = $workFlowTable->find('all',array(
                'conditions' => array(
                    'Workflow.document_part_id' => $docPartId
                ),
                'order' => array('Workflow.id ASC')
            ))->toArray();
            if(!empty($workFlowList)) {
                $response = array('status' => 1, 'workFlowList' => $workFlowList , 'docPartDetail' => $docPartDetails);
            } else {
                $response = array('status' => 1, 'workFlowList' => null , 'docPartDetail' => $docPartDetails, 'error' => 'No workflow found for this workflow');
            }
        } else {
            $response = array('status' => 0, 'workFlowList' => null,'docPartDetail' => null, 'error' => 'No details found for this document part');
        }
        echo json_encode($response);die;
    }

     /**
     * @desc Function responsible to get workflow details of document part and workflows
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 19 October 2015
     */
    public function documentlist() {
        $this->set('title', "Documents | List | Admin");
        //loading modal Documents
        $documents = TableRegistry::get('Documents');
        $documentList = $documents->find('all',array(
            'fields' => array('Documents.id','Documents.name','Documents.status',
                'Documents.version','States.code'),
            'order' => array('Documents.id DESC')
        ))->join([
            'States' => [
            'table' => 'states',
            'type' => 'INNER',
            'conditions' => 'Documents.state = States.id',
            ]
        ])->toArray();//echo "<pre>";print_r($documentList);die;
        $this->set('documentList', $documentList);
        $this->layout = 'loggedinlayout';
    }

    /**
     * @desc Function responsible to generate document
     * @param docid    integer docuemtn id whose details has to be fetched
     * @param tokennum varchar token number by which we will fetch repsonses
     * @return arrayObject
     * @author Suman Khatri on 20th October 2015
     */
    public function generatedcoument() {
        //loading modal Documents
        $documentTable = TableRegistry::get('Documents');
        //getting document details
        $documentDetail = $documentTable->find('all',[
            'conditions' => [
                'id' => $this->request->data['docId']
            ],
            'fields' => ['Documents.id','Documents.associated_questionnaire_id']
        ])->toArray();
        $questionnaire = $documentDetail[0]['associated_questionnaire_id'];
        //getting response for the questionnaire
        $this->loadComponent('Questionnaire');
        $responseDetails = $this->Questionnaire->getResponseDetails($this->request->data['tokenNum'],$questionnaire);
        if(is_array($responseDetails)) {
            $response = array('error' => 1,'message' => $responseDetails['status']);
        } else {
            //getting document parts details
            $documentPartsDetail = $this->getDocumentPartFiles($this->request->data['docId'],'docid');
            if(!empty($documentPartsDetail)) {
                $responseDe = current(current(current((array)json_decode(base64_decode($responseDetails)))));
                $response = $this->generatepdf($documentPartsDetail,(array)$responseDe);
            } else {
                $response = array('error' => 1,'message' => "Ther is no document part to generate document preview");
            }
        }
        echo json_encode($response);die;
    }

    /**
     * @desc Function responsible to generate pdf for document parts
     * @param $documentPartsDetail array array of document parts
     * @param $responseDetails     array array of repsonse
     * @return arrayObject
     * @author Suman Khatri on 20th October 2015
     */
    public function generatepdf($documentPartsDetail,$responseDetails) {
        //print_r($documentPartsDetail);die;
        foreach($documentPartsDetail as $dPart) {
            //making conditional part for document part
            $conditionalVariables = array();
            if($dPart['conditions'] != 1 && !empty($dPart['conditions'])) {
                $conditions = explode("$", $dPart['conditions']);
                if(!empty($conditions) && strpos($dPart['conditions'], '$') !== false) {
                    for ($i=1; $i < count($conditions); $i = $i++) {
                        $string = explode($conditions[$i]," ");
                        $conditionalVariables[] = strtolower($string[0]);
                    }
                    if(!empty($result)) {
                        foreach ($conditionalVariables as $cVariables) {
                            $$cVariables = $responseDetails['$cVariables'];
                        }
                    }
                    $replace = array('if',"IF");
                    $conditionsForIf = str_replace($replace, "", $dPart['conditions']);
                }
            }


            if($dPart['conditions'] == 1 || ($conditionsForIf)) {
                // Check to see if the workflow is exists
                if (!empty($dPart['document_parts_file_name']) && file_exists(TPL_PATH. $dPart['document_parts_file_name'])) {
                    $tmp_filename = $this->generateSmartyText(TPL_PATH.$dPart['document_parts_file_name'],$responseDetails);
                    if(!is_array($tmp_filename)) {
                    $documentParts[] = $tmp_filename;
                    } else {
                        return $tmp_filename;
                    }
                // Check to see if the Static PDF exists
                } else if (!empty($dPart['static_pdf_path']) && file_exists(STATIC_PDF_PATH. $dPart['static_pdf_path'])) {
                    $documentParts[] =  STATIC_PDF_PATH. $dPart['static_pdf_path'];
                } else {
                    return array('error' => 1,'message' => 'Something went wrong while generating document');
                }
            }
        }
        if(!empty($documentParts)) {
            $pdfName = 'doc'.time().'.pdf';
            $ouputFileName = USER_DOC.$pdfName;
            $allPdfs = implode($documentParts, " ");
            echo exec("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=$ouputFileName $allPdfs");
            $this->deleteDocTempPdfs($documentParts);
            return array('error' => 0, 'fileName' => 'webroot/user_docs/'.$pdfName);
        } else {
            return array('error' => 1,'message' => 'There is no document part to generate document');
        }
    }

    /**
     * @desc Function responsible to process tpl file and make html and pdf for respective
     * @param $fileName        string tpl file name which has to be processed
     * @param $responseDetails array  array of repsonse
     * @return arrayObject
     * @author Suman Khatri on 20th October 2015
     */
    public function generateSmartyText($fileName,$responseDetails) {
        $smarty = new Smarty();
        //$smarty->force_compile = true;
        $smarty->debugging = false;
        $smarty->caching = false;
        $smarty->cache_lifetime = 0;
        //pasing response variables to tple file
        foreach($responseDetails as $key=>$value) :
           $smarty->assign($key, $value);
        endforeach;
        $data = $smarty->fetch($fileName);
        
        // MIKE 20151101 - Markdown should be performed after fetching template
        //loading file to process markdown content into html
        $raw_markdown = $data;
        require_once(ROOT .DS. "vendor" . DS . "Michelf" . DS . "Markdown.php");
        $data = Markdown::defaultTransform($raw_markdown);
        
        $docPathPdf = USER_DOC."parts/";
        $htmlFileName = $docPathPdf."file.html";
        //cretes html file for output of tpl file
        $myfile = fopen($htmlFileName, "w") or die("Unable to open file!");
        fwrite($myfile, $data);
        fclose($myfile);
        
        // MIKE 20151101 - Let's see what the original raw markdown looked like too
        $htmlFileNameMD = $docPathPdf."raw_markdown.txt";
        $myfileMD = fopen($htmlFileNameMD, "w") or die("Unable to open file!");
        fwrite($myfileMD, $raw_markdown);
        fclose($myfileMD);
        
        $htmlFileNameMD = $docPathPdf."raw_answers.txt";
        $myfileMD = fopen($htmlFileNameMD, "w") or die("Unable to open file!");
        fwrite($myfileMD, print_r($responseDetails,true));
        fclose($myfileMD);        
        
        $pdfName = $docPathPdf.time().".pdf";
        //cretes pdf file of the html files
        try {
            echo exec("/usr/local/bin/wkhtmltopdf $htmlFileName $pdfName");
        } catch(Exception $e) {
            return array('error' => 1, 'message' => $e->getMessage());
        }
        //returns file name
        return $pdfName;
    }

    /**
     * @desc Function responsible to delete temprarory pdf files
     * @param $documentParts array  array of temp pdf files
     * @return nill
     * @author Suman Khatri on 21st October 2015
     */
    public function deleteDocTempPdfs($documentParts) {
        foreach ($documentParts as $tmpPdfs) {
            $pos = strpos($tmpPdfs, STATICPDF);
            if($pos == '' ) {
               @unlink($tmpPdfs);
            }

        }
    }

    /**
     * @desc Function responsible to get doc part files
     * @param $docId integre id of document for which doc part files has to be fetched
     * @return arrayIterator
     * @author Suman Khatri on 22st October 2015
     */
    private function getDocumentPartFiles($id,$typeOfId) {
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        if($typeOfId == 'docid') {
            $condition['DocumentParts.document_id'] = $id;
        } else if($typeOfId == 'id') {
            $condition['DocumentParts.id'] = $id;
        } else if($typeOfId == 'ids') {
            $condition[] = "DocumentParts.id in ($id)";
        }
        //getting document parts details
        $documentPartsDetail = $documentParts->find('all',array(
            'fields' => array('DocumentParts.id','DocumentParts.document_parts_file_name','DocumentParts.static_pdf_path','DocumentParts.conditions'),
            'conditions' => $condition
        ))->order("DocumentParts.sort_order ASC")->toArray();
        return $documentPartsDetail;
    }

    /**
     * @desc Function responsible to delete docparts
     * @param ids string comma seperated ids of document parts
     * @return arrayObject
     * @author Suman Khatri on 22st October 2015
     */
    public function deletedocpart() {
        $ids = $this->request->data['ids'];
        $documentPartsDetail = $this->getDocumentPartFiles($ids, 'ids');
        foreach($documentPartsDetail as $dPart) {
            // delete if the workflow is exists
            if (!empty($dPart['document_parts_file_name']) && file_exists(TPL_PATH. $dPart['document_parts_file_name'])) {
                @unlink(TPL_PATH.$dPart['document_parts_file_name']);
            // delete if the Static PDF exists
            } elseif (!empty($dPart['static_pdf_path']) && file_exists(STATIC_PDF_PATH. $dPart['static_pdf_path'])) {
                @unlink(STATIC_PDF_PATH. $dPart['static_pdf_path']);
            }
        }
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        $condition[] = "id in ($ids)";
        //deleting doc parts
        $documentPartsDetail = $documentParts->deleteAll($condition);
        echo json_encode(array('error' => 0,'message' => 'Document part(s) deleted successfully'));die;
    }

    /**
     * @desc Function responsible to publish document
     * @param docId int id of document
     * @return arrayObject
     * @author Suman Khatri on 22st October 2015
     */
    public function publishdocument() {
        //loading modal Document
        $documents = TableRegistry::get('Documents');
        $data['Document']['status'] = 3;
        if(isset($this->request->data['docId']) && !empty($this->request->data['docId'])) {
            $doc = $documents->get($this->request->data['docId'], [
                'contain' => []
            ]);
        } else {
            $doc = $documents->newEntity();
        }
        $doc = $documents->patchEntity($doc, $data['Document']);
        $saveDoc = $documents->save($doc);
        echo json_encode(array('error' => 0,'message' => 'Document published successfully'));die;
    }

    /**
     * @desc Function responsible to change order of docparts
     * @param docPartId int    id of document parts whose order has to be changed
     * @param type      string type of action has to be performed
     * @return arrayObject
     * @author Suman Khatri on 23st October 2015
     */
    public function changedocpartorder() {
        $docPartId = $this->request->data['docPartId'];
        $type = $this->request->data['type'];
        //loading modal DocumentPart
        $documentParts = TableRegistry::get('DocumentParts');
        //getting current doc part detail
        $currentDocumentPart = $documentParts->find('all',[
            'fields' => ['DocumentParts.id','DocumentParts.document_id','DocumentParts.sort_order'],
            'conditions' => [
                'DocumentParts.id' => $docPartId
            ]
        ])->toArray();
        $condition['DocumentParts.document_id'] = $currentDocumentPart[0]['document_id'];
        if($type == 'up') {
            $condition['DocumentParts.sort_order <'] = $currentDocumentPart[0]['sort_order'];
        } else {
            $condition['DocumentParts.sort_order >'] = $currentDocumentPart[0]['sort_order'];
        }
        //getting other doc part detail
        $otherDocumentPart = $documentParts->find('all',[
            'fields' => ['DocumentParts.id','DocumentParts.document_id','DocumentParts.sort_order'],
            'conditions' => $condition
        ])->limit(1)->toArray();
        $data['DocumentParts']['sort_order'] = $otherDocumentPart[0]['sort_order'];
        $docPart = $documentParts->get($currentDocumentPart[0]['id'], [
            'contain' => []
        ]);
        $docPart = $documentParts->patchEntity($docPart, $data['DocumentParts']);
        $saveDocPart = $documentParts->save($docPart);
        $data['DocumentParts']['sort_order'] = $currentDocumentPart[0]['sort_order'];
        $docPart = $documentParts->get($otherDocumentPart[0]['id'], [
            'contain' => []
        ]);
        $docPart = $documentParts->patchEntity($docPart, $data['DocumentParts']);
        $saveDocPart = $documentParts->save($docPart);
        echo json_encode(array('error' => 0,'message' => 'Order updated successfully'));die;
    }
    
    public function savepdf() {
        $filesToUpload = $this->request->data['file'];//print_r($filesToUpload);die;
        $tempFile = $filesToUpload['tmp_name'];
        $fileName = time().str_replace(" ", "_", $filesToUpload['name']);
	$targetPath = STATIC_PDF_PATH.$fileName;
	if(move_uploaded_file($tempFile,$targetPath)) {
            echo json_encode(array('error' => 0, 'fileName' => $fileName));die;
        } else {
            echo json_encode(array('error' => 1, 'message' => "File not updated due to some error"));die;
        }
    }
}