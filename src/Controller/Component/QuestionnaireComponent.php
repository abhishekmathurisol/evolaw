<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;
/**
 * @desc Questionnaire component
 */
class QuestionnaireComponent extends Component
{
    private $sessionKey;
    private $myJSONRPCClient;

    public function initialize(array $config) {
        include_once LIME_SURVEY_RPC_PATH;
        define( 'LS_BASEURL', 'http://evolawprototype.dev2.bigideas.io/questionnaire/index.php');  // adjust this one to your actual LimeSurvey URL
        define( 'LS_USER', 'admin' );
        define( 'LS_PASSWORD', 'bytelaunch143' );
        // instanciate a new client
        $this->myJSONRPCClient = new \plugins\jsonrpcphp\JsonRPCClient( LS_BASEURL.'/admin/remotecontrol' );//echo "<pre>";print_r($myJSONRPCClient);die;
        // receive session key
        $this->sessionKey = $this->myJSONRPCClient->get_session_key( LS_USER, LS_PASSWORD );
    }


    /**
     * @desc Function responsible to get questionnaire list from limesurvey
     * @param nill
     * @return arrayIterator
     * @author Suman Khatri on 5th October 2015
     */
    public function getQuestionnaireList() {
        $questionnaireList = $this->myJSONRPCClient->list_surveys($this->sessionKey);
        return $questionnaireList;
    }
    
    /**
     * @desc Function responsible to formate questionnaire list
     * @param $questionnaireList array list of questionnaire
     * @return arrayIterator
     * @author Suman Khatri on 15th October 2015
     */
    public function formatQuestionnaireList($questionnaireList) {
        $formatedList = array();
        foreach ($questionnaireList as $q) {
            if($q['active'] == 'Y') {
                $formatedList[$q['sid']] = $q['surveyls_title'];
            }
        }
        return $formatedList;
    }
    
    /**
     * @desc Function responsible to get questionnaire detail from limesurvey
     * @param $qId integer id of questionnaire whose details has to be fetched
     * @return arrayObject
     * @author Suman Khatri on 12th October 2015
     */
    public function getQuestionnaireDetails($qId) {
        $questions = $this->myJSONRPCClient->list_questions( $this->sessionKey, $qId );
        return $questions;
    }
    
    /**
     * @desc Function responsible to get response detail from limesurvey
     * @param $tokenNumber   varchar token number whose repsponse has to be fetched
     * @param $questionnaire int     survey(questionnaire) id
     * @return jsonObject
     * @author Suman Khatri on 20th October 2015
     */
    public function getResponseDetails($tokenNumber,$questionnaire) {
    
        // $surveyResponse = $this->myJSONRPCClient->export_responses_by_token($this->sessionKey, $questionnaire, 'json', $tokenNumber); // yours
        $surveyResponse = $this->myJSONRPCClient->export_responses_by_token($this->sessionKey, $questionnaire, 'json', $tokenNumber, 'en', 'all', 'code','long'); // Mike 20151102
        
        return $surveyResponse;
        
    }
}