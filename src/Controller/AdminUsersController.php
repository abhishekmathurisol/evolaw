<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Controller\Component\PaginatorComponent;

/**
 * AdminUsers Controller
 *
 * @property \App\Model\Table\AdminUsersTable $AdminUsers
 */
class AdminUsersController extends AppController
{
    public $paginate = [
        'limit' => 2
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow('login','add');
    }
    
    public function login() {
        $this->render('/Layout/login');
        $this->set('title','EvoLaw Admin Login'); 
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('AdminUsers');
        $this->set('adminUsers', $this->paginate($this->AdminUsers));
        $this->set('_serialize', ['adminUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Admin User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('AdminUsers');
        $adminUser = $this->AdminUsers->get($id, [
            'contain' => []
        ]);
        $this->set('adminUser', $adminUser);
        $this->set('_serialize', ['adminUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('AdminUsers');
        $adminUser = $this->AdminUsers->newEntity();
        if ($this->request->is('post')) {
            $adminUser = $this->AdminUsers->patchEntity($adminUser, $this->request->data);
            if ($this->AdminUsers->save($adminUser)) {
                $this->Flash->success(__('The admin user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The admin user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('adminUser'));
        $this->set('_serialize', ['adminUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Admin User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $adminUser = $this->AdminUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $adminUser = $this->AdminUsers->patchEntity($adminUser, $this->request->data);
            if ($this->AdminUsers->save($adminUser)) {
                $this->Flash->success(__('The admin user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The admin user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('adminUser'));
        $this->set('_serialize', ['adminUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Admin User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $adminUser = $this->AdminUsers->get($id);
        if ($this->AdminUsers->delete($adminUser)) {
            $this->Flash->success(__('The admin user has been deleted.'));
        } else {
            $this->Flash->error(__('The admin user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
