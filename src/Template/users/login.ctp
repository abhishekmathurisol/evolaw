<div class="panel signin">
            <div class="panel-heading">
                <h1>Admin Login</h1>
                <h4 class="panel-title">Sign in to the Admin</h4>
            </div>
            <div class="panel-body">
                <?= $this->Form->create() ?>
                    <div class="form-group mb10">
                        <?= $this->Flash->render();?>
                    </div>
                    <div class="form-group mb10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <?= $this->Form->input('username', array('type' => 'text', 'label'=>false,
                                'class' => 'form-control', 'placeholder' => 'Enter Email' )) ?> 
                        </div>
                    </div>
                    <div class="form-group mb10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <?= $this->Form->input('password', array('type' => 'password', 'label'=>false,
                                'class' => 'form-control', 'placeholder' => 'Enter Password' )) ?> 
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->button(__('Login'),array('class' => 'btn btn-success btn-quirk btn-block')); ?>
                    </div>
                <?= $this->Form->end() ?>
                <hr class="invisible">
            </div>
        </div><!-- panel -->