<div class="leftpanel">
    <div class="leftpanelinner">
      <!-- ################## LEFT PANEL PROFILE ################## -->
      <div class="media leftpanel-profile">
        <div class="media-left">
          <a href="#">
            <?= $this->Html->image('admin/default_user.png', array('alt' => 'CakePHP','class'=>'media-object img-circle')); ?>
          </a>
        </div>
        <div class="media-body">
          <h4 class="media-heading"><?= $userName;?></h4>
        </div>
      </div><!-- leftpanel-profile -->
      <ul class="nav nav-tabs nav-justified nav-sidebar">
        <li class="tooltips active" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-ellipsis-h"></i></a></li>
        <li class="tooltips" data-toggle="tooltip" title="Log Out">
            <a href="<?= $this->Url->build(["controller" => "adminusers", "action" => "logout"]); ?>"><i class="fa fa-sign-out"></i></a> 
        </li>
      </ul>
      <div class="tab-content">
        <!-- ################# MAIN MENU ################### -->
        <div class="tab-pane active" id="mainmenu">
          <!--<h5 class="sidebar-title">Favorites</h5>-->
          <ul class="nav nav-pills nav-stacked nav-quirk">
            <li class="active"><a href="<?= $this->Url->build(["controller" => "adminusers", "action" => "dashboard"]); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
            <li class="nav-parent">
              <a href="#"><i class="fa fa-file-text-o"></i> <span>Documents</span></a>
              <ul class="children">
                <li><a href="<?= $this->Url->build(["controller" => "documents", "action" => "list"]); ?>">List Documents</a></li>
                <li><a href="<?= $this->Url->build(["controller" => "documents", "action" => "add"]); ?>">Add Documents</a></li>
              </ul>
            </li>
            <li class="nav-parent">
              <a href="#"><i class="fa-list-alt"></i> <span>Questionnaire</span></a>
              <ul class="children">
                <li><a href="<?= $this->Url->build(["controller" => "questionnaires", "action" => "list"]); ?>">List Questionnaires</a></li>
                <li><a href="#">Add Questionnaire</a></li>
              </ul>
            </li>
            <li><a href="<?= $this->Url->build(["controller" => "reports", "action" => "list"]); ?>"><i class="fa fa-newspaper-o"></i> <span>Reports</span></a></li>
            <li class="nav-parent">
              <a href="#"><i class="fa fa-users"></i> <span>Customers</span></a>
              <ul class="children">
                <li><a href="<?= $this->Url->build(["controller" => "customer", "action" => "list"]); ?>">List Customers</a></li>
                <li><a href="#">Add Customer</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- tab-pane -->
     </div><!-- tab-content -->
    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->
<div class="mainpanel">
    <div class="contentpanel">
