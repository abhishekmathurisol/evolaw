<?php
//echo $this->request->controller;die;
$controller = strtolower($this->request->controller);
$action = strtolower($this->request->action);
    if($controller == 'users' && $action == 'dashboard') 
        $classDashboard = "active"; 
    else
        $classDashboard = ""; 
    if($controller == 'questionnaire') 
        $classQuest = "active"; 
    else
        $classQuest = ""; 
    $this->loadHelper('Flash');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

        <title><?= $title; ?></title>
        <!--INCLUDEING CSS FOR SITE-->
        <?= $this->Html->css('admin/hover.css') ?>
        <?= $this->Html->css('admin/font-awesome.css') ?>
        <?= $this->Html->css('admin/ionicons.css') ?>
        <?= $this->Html->css('admin/toggles-full.css') ?>
        <?php if(($controller == 'questionnaire' && $action == 'questionnairelist')
            || ($controller == 'document' && $action == 'documentlist')) { ?>
        <?= $this->Html->css('admin/dataTables.bootstrap.css') ?>
        <?php } if(($controller == 'questionnaire' && $action == 'questionnairelist') 
            || ($controller == 'document' && $action == 'document')) { ?>
        <?= $this->Html->css('admin/select2.css') ?>
        <?php } ?>
        <?php if(($controller == 'document' && $action == 'document')) { ?>
        <?= $this->Html->css('admin/dropzone.css') ?>
        <?php } ?>
        <?php //echo $this->Url->build('/', true);?>
        <?php //echo ADMINIMG;die; ?>
        <?= $this->Html->css('admin/quirk.css') ?>
        <?= $this->Html->css('admin/developer.css') ?>
        <script>
            var ADMIN_IMAGE_PATH = '<?php echo $this->Url->build('/img/admin/', true);?>';
            //directory seprator is not wokring
            //var ADMIN_IMAGE_PATH = '/BookMarker/img/admin/';
            var SITE_URL = "<?php echo $this->Url->build('/', true);?>";
        </script>
        <!--INCLUDING JS FOR SITE-->
        <?= $this->Html->script('admin/modernizr.js') ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <?= $this->Html->script('admin/html5shiv.js') ?>
        <?= $this->Html->script('admin/respond.src.js') ?>
      <![endif]-->
    </head>
    <body>
        <header>
            <div class="headerpanel">
                <div class="logopanel">
                  <h2><a href="index.html">EvoLaw</a></h2>
                </div><!-- logopanel -->
                <div class="headerbar">
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                    <div class="searchpanel">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div><!-- input-group -->
                    </div>
                </div><!-- headerbar -->
            </div><!-- header-->
        </header>
        <section>
            <div class="leftpanel">
                <div class="leftpanelinner">
                <!-- ################## LEFT PANEL PROFILE ################## -->
                    <div class="media leftpanel-profile">
                        <div class="media-left">
                            <a href="#">
                                <?= $this->Html->image('admin/default_user.png', array('alt' => 'CakePHP','class'=>'media-object img-circle')); ?>
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?= $userName;?></h4>
                        </div>
                    </div><!-- leftpanel-profile -->
                    <ul class="nav nav-tabs nav-justified nav-sidebar">
                        <li class="tooltips active" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-ellipsis-h"></i></a></li>
                        <li class="tooltips" data-toggle="tooltip" title="Log Out">
                            <a href="<?= $this->Url->build(["controller" => "users", "action" => "logout"]); ?>"><i class="fa fa-sign-out"></i></a> 
                        </li>
                    </ul>
                    <div class="tab-content">
                        <!-- ################# MAIN MENU ################### -->
                        <div class="tab-pane active" id="mainmenu">
                            <h5 class="sidebar-title">Navigation</h5>
                            <ul class="nav nav-pills nav-stacked nav-quirk">
                                <li class="<?= $classDashboard;?>"><a href="<?= $this->Url->build(["controller" => "users", "action" => "dashboard"]); ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                                <li class="nav-parent">
                                    <a href="#"><i class="fa fa-file-text-o"></i> <span>Documents</span></a>
                                    <ul class="children">
                                        <li><a href="<?= $this->Url->build(["controller" => "document", "action" => "documentlist"]); ?>">List Documents</a></li>
                                        <li><a href="<?= $this->Url->build(["controller" => "document", "action" => "document"]); ?>">Add Documents</a></li>
                                    </ul>
                                </li>
                                <li class="nav-parent <?= $classQuest;?>">
                                    <a href="#" ><i class="fa-list-alt"></i> <span>Questionnaires</span></a>
                                    <ul class="children">
                                        <li class="<?= $classQuest;?>"><a href="<?= $this->Url->build(["controller" => "questionnaire", "action" => "questionnairelist"]); ?>">List Questionnaires</a></li>
                                        <li><a href="http://evolawprototype.dev2.bigideas.io/questionnaire/index.php/admin/survey/sa/newsurvey" target="_blank">Add Questionnaire</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?= $this->Url->build(["controller" => "reports", "action" => "list"]); ?>"><i class="fa fa-newspaper-o"></i> <span>Reports</span></a></li>
                                <li class="nav-parent">
                                    <a href="#"><i class="fa fa-users"></i> <span>Customers</span></a>
                                    <ul class="children">
                                        <li><a href="<?= $this->Url->build(["controller" => "customer", "action" => "list"]); ?>">List Customers</a></li>
                                        <li><a href="#">Add Customer</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- tab-pane -->
                    </div><!-- tab-content -->
                </div><!-- leftpanelinner -->
            </div><!-- leftpanel -->
            <div class="mainpanel">
                <div class="contentpanel">
                    <?= $this->fetch('content'); ?>
                </div>
            </div>
        </section>
        <?= $this->Html->script('admin/jquery.js') ?>
        <?= $this->Html->script('admin/jquery-ui.js') ?>
        <?= $this->Html->script('admin/bootstrap.js') ?>
        <?= $this->Html->script('admin/toggles.js') ?>
        <?php if(($controller == 'questionnaire' && $action == 'questionnairelist')
                || ($controller == 'document' && $action == 'documentlist')) { ?>
        <?= $this->Html->script('admin/jquery.dataTables.js') ?>
        <?= $this->Html->script('admin/dataTables.bootstrap.js') ?>
        <?php } if(($controller == 'questionnaire' && $action == 'questionnairelist') 
                    || ($controller == 'document' && $action == 'document')) { ?>
        <?= $this->Html->script('admin/select2.js') ?>
        <?php } ?>
        <?php if(($controller == 'document' && $action == 'document')) { ?>
        <?= $this->Html->script('admin/dropzone.js') ?>
            <script>
                Dropzone.autoDiscover = false;
                //Dropzone.destroy();
                var url = SITE_URL+'document/savepdf';
                $(document).ready(function(){
                    var myDropzone = new Dropzone("#staticpdf", 
                    { 
                        clickable: '#staticpdf',
                        maxFiles: 1,
                        uploadMultiple: false,
                        method:'post',
                        url: url,
                        init: function() {
                            this.on("maxfilesexceeded", function(file) {
                                $("#staticpdf-error").remove();
                                $("#staticpdf").removeClass('has-success').addClass('has-error');
                                $("#staticpdf").after('<label id="staticpdf-error" class="error" for="staticpdf">Only one pdf file can be uploaded</label>');
                                this.removeAllFiles();
                            });
                        },
                        accept: function(file, done) {
                            if (file.type != "application/pdf") {
                                this.removeAllFiles(true);
                                $("#staticpdf").removeClass('has-success').addClass('has-error');
                                $("#staticpdf").after('<label id="staticpdf-error" class="error" for="staticpdf">Only pdf file is allowed to upload</label>');
                            }else{
                                $("#staticpdf").removeClass('has-error');
                                $("#staticpdf-error").remove();
                                done();
                            }
                        }
                    });
                    myDropzone.on("success", function(file,responseText) {
                        var data = $.parseJSON(responseText);
                        if(data.error == 1) {
                            alert(data.message);
                        } else {
                            $(".dz-filename").html('<span data-dz-name="">'+data.fileName+'</span>')
                            $("#pdfname").val(data.fileName);
                            myDropzone.disable;
                            $(".dz-hidden-input").prop("disabled",true);
                        }

                   });
               });
            </script>
        <?= $this->Html->script('admin/jquery.validate.js') ?>
        <?php } ?>
        <?= $this->Html->script('admin/quirk.js') ?>
        <?php if(($controller == 'users' && $action == 'dashboard')) {?>
        <?= $this->Html->script('admin/dashboard.js') ?>
        <?php }?>
        <?= $this->Html->script('admin/jquery.blockUI.js') ?>
        <?= $this->Html->script('admin/function.js') ?>
        <?php if(($controller == 'questionnaire' && $action == 'questionnairelist')) { ?>
        <script>
        $(document).ready(function() {

          'use strict';

          $('#dataTable1').DataTable({
            "aaSorting": [],
            "aoColumns": [
              { 'data': 'id' ,"bSortable": false },
              { 'data': 'name', "bSortable": false },
              { 'data': 'status', "bSortable": false }
            ],
            "language": {
                "search": "Filter by:",
                "sSearchPlaceholder": "ID, Name contianing phrase, or Status"
            },
            //"dom": '<"top"i>rt<"bottom"flp><"clear">'
            });
        });
        </script>
        <?php } ?>
        <?php if(($controller == 'document' && $action == 'documentlist')) { ?>
        <script>
        $(document).ready(function() {

          'use strict';

          $('#dataTable1').DataTable({
            "aaSorting": [],
            "aoColumns": [
              { 'data': 'state' ,"bSortable": false },
              { 'data': 'document' },
              { 'data': 'version' },
              { 'data': 'status' },
              { 'data': 'action', "bSortable": false }
            ],
            "language": {
                "search": "Filter by:",
                "sSearchPlaceholder": "Version and Status"
            },
            });
        });
        </script>
        <?php } ?>
        <?php if(($controller == 'document' && $action == 'document')) { ?>
        <script>
        $(document).ready(function() {
            //$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
            $('#documentcategory').select2({ minimumResultsForSearch: Infinity });
        });
        </script>
        <?php } ?>
    </body>
</html>