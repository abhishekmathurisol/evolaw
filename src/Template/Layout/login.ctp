<?php $this->loadHelper('Flash');?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

        <title><?= $title; ?></title>
        <?= $this->Html->css('admin/font-awesome.css') ?>
        <?= $this->Html->css('admin/quirk.css') ?>
        <?= $this->Html->script('admin/modernizr.js') ?>
    </head>
    <body class="signwrapper">
        <div class="sign-overlay"></div>
        <div class="signpanel"></div>
        <?= $this->fetch('content'); ?>
    </body>
</html>