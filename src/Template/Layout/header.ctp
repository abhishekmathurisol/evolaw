<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

  <title><?= $title; ?></title>
    <!--INCLUDEING CSS FOR SITE-->
    <?= $this->Html->css('admin/hover.css') ?>
    <?= $this->Html->css('admin/ionicons.css') ?>
    <?= $this->Html->css('admin/weather-icons.css') ?>
    <?= $this->Html->css('admin/font-awesome.css') ?>
    <?= $this->Html->css('admin/toggles-full.css') ?>
    <?= $this->Html->css('admin/morris.css') ?>
    <?= $this->Html->css('admin/quirk.css') ?>
    <!--INCLUDING JS FOR SITE-->
    <?= $this->Html->script('admin/modernizr.js') ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <?= $this->Html->script('admin/html5shiv.js') ?>
    <?= $this->Html->script('admin/respond.src.js') ?>
  <![endif]-->
</head>

<body>

<header>
  <div class="headerpanel">
    <div class="logopanel">
      <h2><a href="index.html">EvoLaw</a></h2>
    </div><!-- logopanel -->
    <div class="headerbar">
      <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
      <div class="searchpanel">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
          </span>
        </div><!-- input-group -->
      </div>
      <div class="header-right">
        <ul class="headermenu">
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-logged" data-toggle="dropdown">
                <?= $this->Html->image('admin/default_user.png', array('alt' => 'CakePHP')); ?>
                <?= $userName; ?>
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right">
                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                <li><a href="signin.html"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div><!-- header-right -->
    </div><!-- headerbar -->
  </div><!-- header-->
</header>
<section>