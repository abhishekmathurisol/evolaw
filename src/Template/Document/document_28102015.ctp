<?php
if(isset($document[0])) {
$document = $document[0];
}
$i=0;
if(isset($document->version)) {
    $version =  explode('.',$document->version);
}
//echo "<pre>";
//print_r($document);
//print_r($documentparts); die;
?>
<ol class="breadcrumb breadcrumb-quirk">
    <li><a href="<?= $this->Url->build(["controller" => "adminusers", "action" => "dashboard"]); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
    <li><a href="<?= $this->Url->build(["controller" => "document", "action" => "documentlist"]); ?>"> Documents</a></li>
    <li class="active"><?php if(isset($docId) && !empty($docId)){ echo 'Update'; } else { echo 'Add';} ?> Document</li>
</ol>
<div class="row">
    <div class="panel">
      <div class="panel-body">
            <?= $this->Form->create('document', array('class' => 'form-horizontal','id' => 'document', 'enctype' => 'multipart/form-data', 'url' => array('controller' => 'document', 'action' => 'savepdf'))) ?>
            <div class="form-group mb10">
                <?= $this->Flash->render();?>
            </div>
            <div class="col-sm-6 marginLeft1Per">
                <h3 class="marginLeft1Per"><?php if(isset($docId) && !empty($docId)){ echo 'Update'; } else { echo 'Add';} ?> Document</h3>
                <h5 class="marginLeft1Per">General</h5>
            </div>
            <div class="col-sm-6" style="text-align:right;">
                <h4>Document version</h4>
                <div id="versionsBlock">
                    <div class="col-sm-1 floatRight">
                        <div class="form-group">
                            <?= $this->Form->input('version[2]', array(
                                'type' => 'text', 
                                'label'=>false,
                                'class' => 'form-control',
                                'required' => true,
                                'value' => isset($version[2])?$version[2]:'',
                                'tabindex' => '3',
                                'maxlength' => 1)) ?>
                        </div>
                    </div>
                    <div class="col-sm-1 floatRight dotOperator">.</div>
                    <div class="col-sm-1 floatRight">
                        <div class="form-group ">
                            <?= $this->Form->input('version[1]', array(
                                'type' => 'text', 
                                'label'=>false,
                                'class' => 'form-control',
                                'required' => true,
                                'value' => isset($version[1])?$version[1]:'',
                                'tabindex' => '2',
                                'maxlength' => 1)) ?>
                        </div>
                    </div>
                    <div class="col-sm-1 floatRight dotOperator">.</div>
                    <div class="col-sm-1 floatRight">
                        <div class="form-group ">
                            <?= $this->Form->input('version[0]', array(
                                'type' => 'text', 
                                'label'=>false,
                                'class' => 'form-control',
                                'required' => true ,
                                'value' => isset($version[0])?$version[0]:'',
                                'tabindex' => '1',
                                'maxlength' => 1)) ?> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-7">
                    <?= $this->Form->input('documenttitle', array(
                        'type' => 'text', 
                        'label'=>false,
                        'class' => 'form-control', 
                        'placeholder' => 'Enter Document Title',
                        'required' => true,
                        'value' => isset($document->name)?$document->name:'',
                        'tabindex' => '4',)) ?> 
                    <?= 
                        $this->Form->input('docid', array(
                            'type' => 'hidden', 
                            'label'=>false,
                            'class' => 'form-control',
                            'value' => isset($docId)?$docId:'' )) ?>
                    <?= $this->Form->input('status', array(
                            'type' => 'hidden', 
                            'label'=>false,
                            'class' => 'form-control',
                            'value' => isset($document->status)?$document->status:2  )) ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-7">
                    <?php if(isset($document->category_id) && !empty($document->category_id)) {
                        $categoriesArray = explode(",", $document->category_id);
                    }?>
                    <?= $this->Form->input('documentcategory', array(
                        'type' => 'select',
                        'data-placeholder' => 'Select Category',
                        'options' => $categoryList,
                        'class' => 'form-control select2-hidden-accessible',
                        'label' => false,
                        'required' => true,
                        'value' => isset($categoriesArray)?$categoriesArray:'',
                        'tabindex' => '5',
                        'multiple' => true
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <?= $this->Form->input('state', array(
                        'type' => 'select',
                        'empty' => 'Select State',
                        'options' => $stateList,
                        'class' => 'form-control',
                        'label'=>false,
                        'required' => true,
                        'value' => isset($document->state)?$document->state:'',
                        'tabindex' => '6',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9">
                    <?= $this->Form->input('documentdescription', array(
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'label'=>false,
                        'placeholder' => 'Description',
                        'required' => true,
                        'value' => isset($document->description)?$document->description:'',
                        'tabindex' => '7',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-7">
                    <?= $this->Form->input('questionnaire', array(
                        'type' => 'select',
                        'empty' => 'Select Questionnaire',
                        'options' => $questionnaireList,
                        'class' => 'form-control',
                        'label'=>false,
                        'required' => true,
                        'value' => isset($document->associated_questionnaire_id)?$document->associated_questionnaire_id:'',
                        'tabindex' => '8',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5">
                    <?= $this->Form->input('sku', array(
                        'type' => 'text', 
                        'label'=> 'SKU',
                        'class' => 'form-control',
                        'required' => true,
                        'value' => isset($document->sku)?$document->sku:'',
                        'tabindex' => '9',)) ?> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-1">
                    <?= $this->Form->input('price', array(
                        'type' => 'text', 
                        'label'=> 'Price ($)',
                        'class' => 'form-control',
                        'required' => true,
                        'value' => isset($document->s_price)?$document->s_price:'',
                        'tabindex' => '10',)) ?> 
                </div>
            </div>
            <div class="col-sm-12 marginLeft1Per">
                <h5 class="marginLeft1Per">Document Parts</h5>
            </div>
          <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">
                                </th>
                                <th>Description</th>
                                <th>Type</th>
                                <th class="text-center">Template ID</th>
                                <th class="text-center">Condition</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($documentparts)) {$count = count($documentparts);
                                foreach ($documentparts as $dParts) {$id = $dParts['id'];$sortOrder = $dParts['sort_order'];?>
                            <tr>
                                <td class="text-center">
                                    <label class="ckbox ckbox-primary">
                                        <input type="checkbox" value="<?= $dParts['id'];?>" name="checkDocPart"><span></span>
                                    </label>
                                </td>
                                <td><?= $dParts['name']?></td>
                                <td><?= $dParts['workfolwTemplate']['name']?$dParts['workfolwTemplate']['name']:"Static PDF"; ?></td>
                                <td class="text-center"><?= $dParts['workflow_template_id']; ?></td>
                                <td class="text-right"><?= $dParts['conditions']; ?></td>
                                <td>
                                    <ul class="table-options">
                                        <li><a href="javascript:void(0)" onclick="getDocPartWorkflows(<?= $dParts['id']?>);"><i class="fa fa-pencil"></i></a></li>
                                        <?php if($i > 0) {?>
                                        <li><?= $this->Html->image('admin/arrow-up.png', array('alt' => 'CakePHP','class'=>'clickAble','onclick' => "moveDocPart($id,'up');", 'id' => "moveupdoc$i", 'title' => "Move up", 'disabled' => 'disabled')); ?></li>
                                        <?php } if($i < $count-1) {?>
                                        <li><?= $this->Html->image('admin/arrow-down.png', array('alt' => 'CakePHP','class'=>'clickAble','onclick' => "moveDocPart($id,'down');", 'id' => "movedowndoc$i", 'title' => "Move down", 'disabled' => 'disabled')); ?></li>
                                        <?php }?>
                                    </ul>
                                </td>
                            </tr>
                            <?php $i++;}} else {?>
                                <tr>
                                <td colspan="6" class="text-center">No Document Parts</td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            <?php if($i > 1) {?>
            <div class="form-group">
                <?= $this->Form->label('withselect', 'With selected',['class' => 'floatLeft classPadding10']); ?>
                <div class="col-sm-3">
                    <?= 
                        $this->Form->input('withselect', array(
                        'type' => 'select',
                        'empty' => 'Select Action',
                        'options' => array('delete' => 'Delete'),
                        'class' => 'form-control',
                        'label'=> false,
                        'onchange' => "deleteDocPart(this.value);",
                        'required' => true ,
                        'tabindex' => '11',
                    )) ?> 
                </div>
            </div>
            <?php } ?>
            <div class="col-sm-8 marginLeft1Per">
                <h5 class="marginLeft1Per">Add new document part</h5>
            </div>
            <fieldset class="col-sm-12 fieldsetDocPartWorkFlow">
                <div class="form-group">
                    <div class="col-sm-5">
                        <?= $this->Form->input('documentpartdesc', array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder'=> 'Description',                        
                            'label'=>false,
                            'required' => true,
                            'tabindex' => '12', 
                        )); ?>
                        <?= $this->Form->input('sortorder', array(
                                'type' => 'hidden',
                                'value' => isset($sortOrder)?$sortOrder+1:1
                            ));?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                        <?= $this->Form->input('documentparttype', array(
                            'type' => 'select',
                            'empty' => 'Select Type',
                            'options' => $documentPartTypeList,
                            'class' => 'form-control',
                            'label'=> false,
                            'onchange' => "showUploadDropBox(this.value);",
                            'required' => true,
                            'tabindex' => '13', 
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <?= $this->Form->label('condition', 'Add condition');?>
                        <?= $this->Html->image('admin/green_add.png', array('alt' => 'CakePHP', 'class'=>'clickAble', 'id' => 'addcondition', 'onclick' => "showHideConditionBox();", 'height' => '16')); ?><br/>
                        <?= $this->Form->input('condition', array(
                            'type' => 'text',
                            'class' => 'form-control hiddenElement',
                            'label'=> false,
                            'required' => true ,
                            'disabled' => true,
                            'tabindex' => '14',
                        )); ?>
                    </div>
                </div>
                <div class="form-group hiddenElement" id="staticpdfdiv">
                    <div class="col-sm-4 fallback">
                        <?= $this->Form->input('staticpdf', array(
                            'type' => 'file', 
                            'label'=> 'Click or Drop File',
                            'class' => 'dropzone form-control',
                            'label'=> 'Static PDF',
                            'disabled' => true,
                            'tabindex' => '15',
                        )) ?> 
                        <?= $this->Form->input('pdfname', array(
                            'type' => 'hidden',
                            'value' => '',
                        )) ?> 
                    </div>
                </div>
                <div id="workFlowDiv" class ="hiddenElement">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <?= $this->Form->input('template', array(
                                'type' => 'select',
                                'empty' => 'Select Template',
                                'options' => $workflowTemplatesList,
                                'class' => 'form-control',
                                'label'=> false,
                                'required' => true,
                                'disabled' => true,
                                'tabindex' => '16',
                            )); ?>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped" border="1px">
                        <thead>
                            <tr>
                                <th width="20%">Type</th>
                                <th width="30%">Condition</th>
                                <th width="40%">Markdown</th>
                                <th class="text-center"  width="10%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="documentpartworkflow1">
                                <td valign="middle">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <?= $this->Form->input('typeofworkflow1', array(
                                                'type' => 'text',
                                                'class' => 'form-control',
                                                'label'=> false,
                                                'placeholder' => '',
                                                'required' => true,
                                                'disabled' => 'disabled',
                                                'tabindex' => '17',
                                            )) ?> 
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <?= $this->Form->input('workflowcondition1', array(
                                                'type' => 'text', 
                                                'label'=>false,
                                                'class' => 'form-control', 
                                                'placeholder' => '',
                                                'required' => true,
                                                'disabled' => 'disabled',
                                                'tabindex' => '18',)) ?>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <?= $this->Form->input('workflowmarkdown1', array(
                                            'type' => 'textarea', 
                                            'label'=>false,
                                            'class' => 'form-control', 
                                            'rows' => 2,
                                            'placeholder' => '',
                                            'required' => true,
                                            'disabled' => 'disabled',
                                            'tabindex' => '19',)) ?>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center" id="action1">
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan = "4">
                                    <?= $this->Html->image('admin/green_add.png', array('alt' => 'CakePHP','class'=>'clickAble','onclick' => "addRow();", 'title' => "Add Row", 'height' => '16', 'id' => "addrow", 'disabled'=> 'disabled')); ?>&nbsp;&nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br/><br/>
                    <?= $this->Form->input('numofrows', array('type' => 'hidden','label'=>false, 'value'=>"1")); ?>
                </div>
                <div class="col-sm-3 floatRight">
                    <div class="form-group floatRight">
                        <?= $this->Form->button('Save', array('type' => 'button',
                            'class' => 'btn btn-info', 'id' => 'savepart', 'onclick' => "validateDocument('savepart');"));?>
                    </div>
                </div><br/>
            </fieldset>
            <div class="clearfix"></div><br/>
            <div class="col-sm-2"></div>
            <div class="col-sm-3">
                <div class="form-group">
                    <?= $this->Form->button('Save as draft', array('type' => 'button',
                        'class' => 'btn btn-wide btn-primary btn-quirk mr5', 'id' => 'saveasdraft', 
                        'onclick' => "validateDocument('saveasdraft');"));?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <?php if(isset($docId) && !empty($docId)) {?>
                        <?= $this->Form->button('Preview', array('type' => 'button',
                        'class' => 'btn btn-wide btn-primary btn-quirk mr5' , 'id' => 'preview', 
                        'onclick' => "preViewDoc($docId);"));?>
                    <?php } else { ?>
                        <?= $this->Form->button('Preview', array('type' => 'button',
                        'class' => 'btn btn-wide btn-primary btn-quirk mr5' , 'id' => 'preview', 'disabled' => 'disabled'));?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <?php if(isset($docId) && !empty($docId)) {?>
                        <?= $this->Form->button('Publish Version', array('type' => 'button',
                        'class' => 'btn btn-wide btn-primary btn-quirk mr5' , 'id' => 'preview', 
                        'onclick' => "publishVersion($docId);"));?>
                    <?php } else { ?>
                        <?= $this->Form->button('Publish Version', array('type' => 'button',
                        'class' => 'btn btn-wide btn-primary btn-quirk mr5' , 'id' => 'preview', 'disabled' => 'disabled'));?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div><!-- panel -->
    <?= $this->Form->end() ?>
</div>
<!-- $this->Form->label('name', '<span class="mandatory">*</span> Name'); -->