<ol class="breadcrumb breadcrumb-quirk">
    <li><a href="<?= $this->Url->build(["controller" => "users", "action" => "dashboard"]); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
    <li class="active">Documents</li>
</ol>
<div class="panel">
    <div class="panel-heading">
      <h4 class="panel-title">List Documents</h4>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="dataTable1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>State</th>
                        <th>Document</th>
                        <th>Version</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($documentList)) { foreach($documentList as $dList){?>
                    <tr>
                        <td><?= $dList['States']['code'];?></td>
                        <td><?= $dList['name'];?></td>
                        <td><?= $dList['version'];?></td>
                        <td><?php if($dList['status'] == 2) { echo "Draft";} else {echo "Published";}?></td>
                        <td>
                            <?php if($dList['status'] == 2) {?>
                                <a href="<?= $this->Url->build(["controller" => "document", "action" => "document", 'id' => $dList['id']]); ?>", title="Edit document">Edit</a>
                            <?php } else {?>
                                <a href="javascript:void(0)" onclick="preViewDoc(<?php echo $dList['id']; ?>);" title="View document">View</a>
                            <?php }?>
                        </td>
                    </tr>
                    <?php } } else {?> 
                    
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><!-- panel -->