<ol class="breadcrumb breadcrumb-quirk">
    <li><a href="<?= $this->Url->build(["controller" => "users", "action" => "dashboard"]); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
    <li class="active">List Questionnaires</li>
  </ol>
    <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">List Questionnaires</h4>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="dataTable1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($questionnaireList)) { foreach($questionnaireList as $qList){?>
                <tr>
                  <td><?= $qList['sid'];?></td>
                  <td>
                        <?= $qList['surveyls_title'];?>
                        <?= $this->Html->link($this->Html->image('admin/redirect.png',array('alt'=>'redirect')),"http://evolawprototype.dev2.bigideas.io/questionnaire/index.php/admin/survey/sa/view/surveyid/".$qList['sid'],array('target' => '_blank','escape' => false)); ?>
                  </td>
                  <td><?php if($qList['active'] == 'Y') { echo "Active";} else {echo "Not Active";}?></td>
                </tr>
                <?php } } else {?> 
                <tr>
                  <td collspan="3">No Record Found</td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div><!-- panel -->