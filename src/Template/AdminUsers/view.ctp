<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Admin User'), ['action' => 'edit', $adminUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Admin User'), ['action' => 'delete', $adminUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adminUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Admin Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Admin User'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="adminUsers view large-10 medium-9 columns">
    <h2><?= h($adminUser->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($adminUser->email) ?></p>
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($adminUser->username) ?></p>
            <h6 class="subheader"><?= __('Password') ?></h6>
            <p><?= h($adminUser->password) ?></p>
            <h6 class="subheader"><?= __('Role') ?></h6>
            <p><?= h($adminUser->role) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($adminUser->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($adminUser->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($adminUser->modified) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Status') ?></h6>
            <p><?= $adminUser->status ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
</div>
