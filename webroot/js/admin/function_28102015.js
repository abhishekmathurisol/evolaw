//function to show static pdf upload drop box on add/edit document
function showUploadDropBox(val) {
    if(val == 3) {
        $( "#staticpdfdiv" ).removeClass( "hiddenElement" );
        $( "#staticpdf" ).removeAttr( "disabled" );
        removeAllWorkFlow();
        $( "#workFlowDiv" ).removeClass( "hiddenElement" ).addClass("hiddenElement");
        $( "#workFlowDiv :input" ).removeAttr( "disabled" ).attr( "disabled" );
    } else {
        removeStaticPdf();
        $( "#staticpdfdiv" ).removeClass( "hiddenElement" ).addClass("hiddenElement");
        $( "#staticpdf" ).removeAttr( "disabled" ).attr( "disabled" );
        $( "#workFlowDiv" ).removeClass( "hiddenElement" );
        $( "#workFlowDiv :input" ).removeAttr( "disabled" );
        $( "#workFlowDiv :input" ).closest('.form-group').removeClass('has-error');
    }
}

//function to show condition box
function showHideConditionBox() {
    if($( "#condition" ).hasClass( "hiddenElement" ) == true) {
        $( "#condition" ).removeClass( "hiddenElement" );
        $( "#condition" ).removeAttr( "disabled" )
        $('#addcondition').attr('src',ADMIN_IMAGE_PATH+'red_minus.png');
    } else {
        $( "#condition" ).val('');
        $( "#condition" ).removeAttr( "disabled" ).attr( "disabled", "disabled");
        $( "#condition" ).addClass( "hiddenElement" );
        $('#addcondition').attr('src',ADMIN_IMAGE_PATH+'green_add.png');
    }  
}

//function to add row in document part workflow parts
function addRow() {
    var row = $("#numofrows").val();
    var newRow = Number(row)+Number(1);
    var html = '<tr id="documentpartworkflow'+newRow+'">';
    html += '<td valign="middle"><div class="form-group"><div class="col-sm-12">'; 
    html += '<input type="text" id="typeofworkflow'+newRow+'" name="typeofworkflow'+newRow+'" class="form-control" placeholder=""></div></div></td>';
    html += '<td><div class="form-group"><div class="col-sm-12">';
    html += '<input type="text" id="workflowcondition'+newRow+'" name="workflowcondition'+newRow+'" class="form-control" placeholder=""></div></div></td>';
    html += '<td><div class="form-group"><div class="col-sm-12">';
    html += '<textarea id="workflowmarkdown'+newRow+'" name="workflowmarkdown'+newRow+'" class="form-control" placeholder=""></textarea></div></div></td>';
    html += '<td class="text-center" id="action'+newRow+'"><ul class="table-options">';
    html += '<li><img src="'+ADMIN_IMAGE_PATH+'arrow-up.png" id="moveup'+newRow+'" class="clickAble" alt="CakePHP" title="Move up" onclick="moveUp('+newRow+')"></li>';
    html += '<li><img src="'+ADMIN_IMAGE_PATH+'arrow-down.png" id="movedown'+newRow+'" class="clickAble" alt="CakePHP" title="Move down" onclick="moveDown('+newRow+')"></li>';
    html += '<li><img src="'+ADMIN_IMAGE_PATH+'red_minus.png" id="delete'+newRow+'" class="clickAble" alt="CakePHP" title="Delete" onclick="deleteRow('+newRow+')" height="16"></li></ul></td></tr>';
  
    $("#documentpartworkflow"+row).after(html); 
    $("#numofrows").val(newRow);
    if(row == 1) {
        var htmlAction = '<ul class="table-options"><li><img src="'+ADMIN_IMAGE_PATH+'arrow-up.png" id="moveup'+row+'" class="clickAble" alt="CakePHP" title="Delete" onclick="moveUp('+row+')" height="16"></li>';
        htmlAction += '<li><img src="'+ADMIN_IMAGE_PATH+'arrow-down.png" id="movedown'+row+'" class="clickAble" alt="CakePHP" title="Delete" onclick="moveDown('+row+')" height="16"></li>';
        htmlAction += '<li><img src="'+ADMIN_IMAGE_PATH+'red_minus.png" id="delete'+row+'" class="clickAble" alt="CakePHP" title="Delete" onclick="deleteRow('+row+')" height="16"></li></ul>';
        $( "#action"+row ).html(htmlAction);
    }
    $("#addrow").focus();
}
//function to delete row for document part workflow 
function deleteRow(row){
    var numrow = $("#numofrows").val();
    $("#documentpartworkflow"+row).remove();
    if(numrow > row) {
        for (i=numrow;i>row;i--) {
            j = i-1;
            $("#documentpartworkflow"+i).attr('id', "documentpartworkflow"+j);
            $("#typeofworkflow"+i).attr('id', "typeofworkflow"+j);
            $("#workflowcondition"+i).attr('id', "workflowcondition"+j);
            $("#workflowmarkdown"+i).attr('id', "workflowmarkdown"+j);
            $("#action"+i).attr('id', "action"+j);
            $("#moveup"+i).attr('onclick', "moveUp("+j+")");
            $("#movedown"+i).attr('onclick', "moveDown("+j+")");
            $("#delete"+i).attr('onclick', "deleteRow("+j+")");
            $("#moveup"+i).attr('id', "moveup"+j);
            $("#movedown"+i).attr('id', "movedown"+j);
            $("#delete"+i).attr('id', "delete"+j);
        }
    }
    $("#numofrows").val($("#numofrows").val() - 1);
    if($("#numofrows").val() == 1) {
        $( "#action1" ).html('');
    }
}
//function to move up row of document part workflow 
function moveUp(row) {
    upperRow = row-1;
    if(row > 1) {
        typeofworkflow = $("#typeofworkflow"+row).val();
        workflowcondition = $("#workflowcondition"+row).val();
        workflowmarkdown = $("#workflowmarkdown"+row).val();
        $("#typeofworkflow"+row).val($("#typeofworkflow"+upperRow).val());
        $("#workflowcondition"+row).val($("#workflowcondition"+upperRow).val());
        $("#workflowmarkdown"+row).val($("#workflowmarkdown"+upperRow).val());
        $("#typeofworkflow"+upperRow).val(typeofworkflow);
        $("#workflowcondition"+upperRow).val(workflowcondition);
        $("#workflowmarkdown"+upperRow).val(workflowmarkdown);
    } else {
        alert("You can't move first row up.");
    }
}

//function to move down row of document part workflow 
function moveDown(row) {
    numrow = $("#numofrows").val();
    lowerRow = Number(row)+Number(1);
    if(row < numrow) {
        typeofworkflow = $("#typeofworkflow"+row).val();
        workflowcondition = $("#workflowcondition"+row).val();
        workflowmarkdown = $("#workflowmarkdown"+row).val();
        $("#typeofworkflow"+row).val($("#typeofworkflow"+lowerRow).val());
        $("#workflowcondition"+row).val($("#workflowcondition"+lowerRow).val());
        $("#workflowmarkdown"+row).val($("#workflowmarkdown"+lowerRow).val());
        $("#typeofworkflow"+lowerRow).val(typeofworkflow);
        $("#workflowcondition"+lowerRow).val(workflowcondition);
        $("#workflowmarkdown"+lowerRow).val(workflowmarkdown);
    } else {
        alert("You can't move last row down.");
    }
}
//function to get questionnaire details
function getQuestionnaireDetails() {
    var returnData = '';
    var id = $("#questionnaire").val();
    blockUi();
    var url = SITE_URL+'questionnaire/getquestionnairedetails';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'qid':id},
        async: false,
        success: function(data) {
            returnData = JSON.parse(data);
            $.unblockUI();
        },
        error: function(e) {
        }
    });
    return returnData;   
}
//function to validate add and edit document section
function validateDocument(requestfrom){
    var err = 0;
    var codeList = '';
    $(".error").remove();
    $('.form-group').removeClass('has-error');
    var emptyErrorMessage = 'This field is required';
    if( (requestfrom == 'saveasdraft') || (requestfrom == 'savepart' && $("#docid").val() == '')) {
        if($.trim($("#version-0").val()) == '' || 
            $.trim($("#version-1").val()) == '' ||
                $.trim($("#version-2").val()) == '') {
            err = 1;
            $("#version-0").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#version-1").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#version-2").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#versionsBlock").append('<label id="version-error" class="error" for="version">'+emptyErrorMessage+'&nbsp;&nbsp;</label>');
        } else {
            err = 0;
            $("#version-0").closest('.form-group').removeClass('has-error');
            $("#version-1").closest('.form-group').removeClass('has-error');
            $("#version-2").closest('.form-group').removeClass('has-error');
            $("version-error").remove();
        }
        if($.trim($("#documenttitle").val()) == '') {
            err = 1;
            $("#documenttitle").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#documenttitle").closest('.input').parent().append('<label id="documenttitle-error" class="error" for="documenttitle">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#documenttitle").closest('.form-group').removeClass('has-error');
            $("#documenttitle-error").remove();
        }
        if($.trim($("#documentcategory").val()) == '') {
            err = 1;
            $("#documentcategory").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#documentcategory").closest('.input').parent().append('<label id="documentcategory-error" class="error" for="documentcategory">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#documentcategory").closest('.form-group').removeClass('has-error');
            $("#documentcategory-error").remove();
        }
        if($.trim($("#state").val()) == '') {
            err = 1;
            $("#state").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#state").closest('.input').parent().append('<label id="state-error" class="error" for="state">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#state").closest('.form-group').removeClass('has-error');
            $("#state-error").remove();
        }
        if($.trim($("#documentdescription").val()) == '') {
            err = 1;
            $("#documentdescription").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#documentdescription").closest('.input').parent().append('<label id="documentdescription-error" class="error" for="documentdescription">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#documentdescription").closest('.form-group').removeClass('has-error');
            $("#documentdescription-error").remove();
        }
        if($.trim($("#questionnaire").val()) == '') {
            err = 1;
            $("#questionnaire").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#questionnaire").closest('.input').parent().append('<label id="questionnaire-error" class="error" for="questionnaire">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#questionnaire").closest('.form-group').removeClass('has-error');
            $("#questionnaire-error").remove();
        }
        if($.trim($("#sku").val()) == '') {
            err = 1;
            $("#sku").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#sku").closest('.input').parent().append('<label id="sku-error" class="error" for="sku">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#sku").closest('.form-group').removeClass('has-error');
            $("#sku-error").remove();
        }
        if($.trim($("#price").val()) == '' || 
            !$.isNumeric($.trim($("#price").val())) || 
                !$.isNumeric($.trim($("#price").val()))) {
            err = 1;
            $("#price").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#price").closest('.input').parent().parent().after('<label id="price-error" class="error" for="price">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#price").closest('.form-group').removeClass('has-error');
            $("#price-error").remove();
        }
    }
    if(($("#docid").val() == '' && requestfrom == 'saveasdraft') || requestfrom == 'savepart') {
        if($.trim($("#documentpartdesc").val()) == '') {
            err = 1;
            $("#documentpartdesc").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#documentpartdesc").closest('.input').parent().append('<label id="documentpartdesc-error" class="error" for="documentpartdesc">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#documentpartdesc").closest('.form-group').removeClass('has-error');
            $("#documentpartdesc-error").remove();
        }
        if($.trim($("#documentparttype").val()) == '') {
            err = 1;
            $("#documentparttype").closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#documentparttype").closest('.input').parent().append('<label id="documentparttype-error" class="error" for="documentparttype">'+emptyErrorMessage+'</label>');
        } else {
            err = 0;
            $("#documentparttype").closest('.form-group').removeClass('has-error');
            $("#documentparttype-error").remove();
        }
        if($.trim($("#documentparttype").val()) == 3) {
            if($.trim($("#staticpdf").val()) == '') {
                err = 1;
                $("#staticpdf").closest('.form-group').removeClass('has-success').addClass('has-error');
                $("#staticpdf").closest('.input').parent().append('<label id="staticpdf-error" class="error" for="staticpdf">Please upload a pdf</label>');
            } else {
                err = 0;
                $("#staticpdf").closest('.form-group').removeClass('has-error');
                $("#staticpdf-error").remove();
            }
        }
        if($( "#condition" ).hasClass( "hiddenElement" ) == false) {
            if($.trim($("#condition").val()) == '') {
                err = 1;
                $("#condition").closest('.form-group').removeClass('has-success').addClass('has-error');
                $("#condition").closest('.input').parent().append('<label id="condition-error" class="error" for="condition">'+emptyErrorMessage+'</label>');
            } else {
                if($.trim($("#condition").val()) != 'always') {
                    if($.trim($("#condition").val()).indexOf('$') !== -1) {
                        var myArray = $.trim($("#condition").val()).split('$');
                        var result = checkCondition(myArray);
                        if(result == false) {
                            err = 1;
                            $("#condition").closest('.input').parent().append('<label id="condition-error" class="error" for="condition">Condition variable(s) not related to selected questionnaire</label>');
                            $("#condition").closest('.form-group').removeClass('has-success').addClass('has-error');
                        } else {
                            err = 0;
                            $("#condition").closest('.form-group').removeClass('has-error');
                            $("#condition-error").remove();
                        }
                    } else {
                        err = 1;
                        $("#condition").closest('.form-group').removeClass('has-success').addClass('has-error');
                        $("#condition").closest('.input').parent().append('<label id="condition-error" class="error" for="condition">Please add condition variables followed by "$" sign</label>');
                    }
                } else {
                    err = 0;
                    $("#condition").closest('.form-group').removeClass('has-error');
                    $("#condition-error").remove();
                }
            }
        }
        if($.trim($("#documentparttype").val()) != 3 && $.trim($("#documentparttype").val()) != '') {
            if($.trim($("#template").val()) == '') {
                err = 1;
                $("#template").closest('.form-group').removeClass('has-success').addClass('has-error');
                $("#template").closest('.input').parent().append('<label id="template-error" class="error" for="template">'+emptyErrorMessage+'</label>');
            } else {
                err = 0;
                $("#template").closest('.form-group').removeClass('has-error');
                $("#condition-error").remove();
            }
            var numofrows = $("#numofrows").val();
            for (j=1;j<=numofrows;j++) {
                if($.trim($("#typeofworkflow"+j).val()) == '') {
                    err = 1;
                    $("#typeofworkflow"+j).closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#typeofworkflow"+j).closest('.input').parent().append('<label id="typeofworkflow-error'+j+'" class="error" for="workflow">'+emptyErrorMessage+'</label>');
                } else {
                    err = 0;
                    $("#typeofworkflow"+j).closest('.form-group').removeClass('has-error');
                    $("#typeofworkflow-error"+j).remove();
                }
                if($.trim($("#workflowcondition"+j).val()) == '') {
                    err = 1;
                    $("#workflowcondition"+j).closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#workflowcondition"+j).closest('.input').parent().append('<label id="workflowcondition-error'+j+'" class="error" for="workflowcondition">'+emptyErrorMessage+'</label>');
                } else {
                    if($.trim($("#workflowcondition"+j).val()) != 'always') {
                        if($.trim($("#workflowcondition"+j).val()).indexOf('$') !== -1) {
                            var myArray = $.trim($("#workflowcondition"+j).val()).split('$');
                            var result = checkCondition(myArray);
                            if(result == false) {
                                err = 1;
                                $("#workflowcondition"+j).closest('.form-group').removeClass('has-success').addClass('has-error');
                                $("#workflowcondition"+j).closest('.input').parent().append('<label id="workflowcondition-error'+j+'" class="error" for="workflowcondition">Variable you are using for condition is not associated with the selected questionnaire.</label>');
                            } else {
                                err = 0;
                                $("#workflowcondition"+j).closest('.form-group').removeClass('has-error');
                                $("#workflowcondition-error"+j).remove();
                            }
                        } else {
                            err = 1;
                            $("#workflowcondition"+j).closest('.form-group').removeClass('has-success').addClass('has-error');
                            $("#workflowcondition"+j).closest('.input').parent().append('<label id="workflowcondition-error'+j+'" class="error" for="workflowcondition">Please add condition variables followed by "$" sign</label>');
                        }
                    } else {
                        err = 0;
                        $("#workflowcondition"+j).closest('.form-group').removeClass('has-error');
                        $("#workflowcondition-error"+j).remove();
                    }
                }
                if($.trim($("#workflowmarkdown"+j).val()) == '') {
                    err = 1;
                    $("#workflowmarkdown"+j).closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#workflowmarkdown"+j).closest('.input').parent().append('<label id="workflowmarkdown-error'+j+'" class="error" for="template">'+emptyErrorMessage+'</label>');
                } else {
                    err = 0;
                    $("#workflowmarkdown"+j).closest('.form-group').removeClass('has-error');
                    $("#workflowmarkdown-error"+j).remove();
                }
            }
        } else {
            var file = $("#staticpdf").val();
            var get_ext = file.split('.').reverse()[0].toLowerCase();
            if(get_ext != 'pdf') {
                err = 1;
                $("#staticpdf").closest('.form-group').removeClass('has-error').addClass('has-error');
                $("#staticpdf").closest('.input').parent().append('<label id="staticpdf-error" class="error" for="staticpdf">Only pdf file is allowed to upload</label>');
            } else {
                err = 0;
                $("#staticpdf").closest('.form-group').removeClass('has-error');
                $("#staticpdf-error").remove();
                savepdf();
            }
        }
    }
    if(err == 0 && $( ".form-group" ).hasClass( "has-error" ) == false) {
        var str = $( "#document" ).serialize();
        saveDocument(str,requestfrom);
    } else {
        return false;
    }
}
//function to save pdf 
function savepdf() {
    var file = $("#staticpdf").val();
            var get_ext = file.split('.').reverse()[0].toLowerCase();
            if(get_ext != 'pdf') {
                err = 1;
                $("#staticpdf").closest('.form-group').removeClass('has-error').addClass('has-error');
            } else {
            }
}
//function to check conditions
function checkCondition(myArray){
    var questionnaireData = getQuestionnaireDetails();console.log(questionnaireData);
    var codeListData = questionnaireData.codeList;
    var codeList = Object.keys(codeListData).map(function (key) {return codeListData[key]});
    var separators = [' ','!=','=='];
    for (x in myArray) {
        if(x > 0) {
            var condi = myArray[x].split(new RegExp(separators.join('|'), ''))[0];
            if(codeList.indexOf(condi) == -1) {
                return false;
            }
        } 
    }
    return true;
}
//function to save data of document
function saveDocument(str,requestfrom) {
    blockUi();
    var url = SITE_URL+'document/save';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'data':str,'requestfrom':requestfrom},
        async: false,
        success: function(data) {
            data = JSON.parse(data);$.unblockUI();
            if(data.status == 1) {
                $("#docid").val(data.docId);
                if(requestfrom == 'saveasdraft') {
                    var urldoc = SITE_URL+'document/documentlist';
                    window.location = urldoc;
                } else if(requestfrom == 'savepart') {
                    var urldoc = SITE_URL+'document/document?id='+data.docId;
                    window.location = urldoc;
                } 
            } else {
                alert(data.message);
            }
        },
        error: function(e) {
        }
    });
}
//function to get details of document part workflows for edit
function getDocPartWorkflows(id) {
	$(".error").remove();
	$('.form-group').removeClass('has-error');
    blockUi();
    var url = SITE_URL+'document/getdocpartworkflow';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'id':id},
        async: false,
        success: function(data) {
            removeAllWorkFlow();
            removeStaticPdf();
            $( "#docpartid" ).remove();
            $( "#workFlowDiv" ).removeClass( "hiddenElement" ).addClass( "hiddenElement" );
            $( "#workFlowDiv :input" ).removeAttr( "disabled" ).attr( "disabled" );
            var data = $.parseJSON(data);
            $("#documentpartdesc").val(data['docPartDetail'][0]['name']);
            $("#documentpartdesc").after('<input type="hidden" id="docpartid" name="docpartid" value="'+data['docPartDetail'][0]['id']+'">');
            $("#documentparttype").val(data['docPartDetail'][0]['document_part_type_id']);
            $("#sortorder").val(data['docPartDetail'][0]['sort_order']);
            $("#template").val(data['docPartDetail'][0]['workflow_template_id']);
            if(data['docPartDetail'][0]['conditions'] != 1) {
                $( "#condition" ).removeClass( "hiddenElement" );
                $( "#condition" ).removeAttr( "disabled" )
                $( "#condition" ).val( data['docPartDetail'][0]['conditions'] );
                $( '#addcondition' ).attr('src',ADMIN_IMAGE_PATH+'red_minus.png');
            }
            var condi = '';
            $("#documentparttype").val(data['docPartDetail'][0]['document_part_type_id']);
            if(data['docPartDetail'][0]['document_part_type_id'] != 3) {
                $( "#workFlowDiv" ).removeClass( "hiddenElement" );
                $( "#workFlowDiv :input" ).removeAttr( "disabled" );
                for (x in data['workFlowList']) {
                    var inputFieldNo = Number(x)+Number(1);
                    if(x > 0) {
                        addRow(inputFieldNo);
                    } 
                    $("#typeofworkflow"+inputFieldNo).val(data['workFlowList'][x]['section']);
                    if(data['workFlowList'][x]['conditions'] == 1) {
                        condi = 'always';
                    } else {
                        condi = data['workFlowList'][x]['conditions'];
                    }
                    $("#workflowcondition"+inputFieldNo).val(condi);
                    $("#workflowmarkdown"+inputFieldNo).val(data['workFlowList'][x]['content']);
                }
            } else {
                $("#pdfname").val(data['docPartDetail'][0]['static_pdf_path']);
                showUploadDropBox(3);
            }
            $.unblockUI();
        },
        error: function(e) {
        }
    });
}
//function to remove all input fields for workflow except row 1 
function removeAllWorkFlow() {
    var numrow = $("#numofrows").val();
    for(i = numrow; i > 1;i--) {
    $("#documentpartworkflow"+i).remove();
    }
    $("#action1").html('')
    $("#numofrows").val(1);
    $("#typeofworkflow1").val('');
    $("#workflowcondition1").val('');
    $("#workflowmarkdown1").val('');
}
//function to remove staticpdf
function removeStaticPdf() {
    $("#pdfname").val('');
    $( "#staticpdfdiv" ).removeClass( "hiddenElement" ).addClass("hiddenElement");
    $( "#staticpdf" ).removeAttr( "disabled" ).attr( "disabled" );
}
//function for preview
function preViewDoc(docId) {
    var tokenNum = window.prompt("Enter token number", "Token number");
    blockUi();
    var url = SITE_URL+'document/generatedcoument';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'docId':docId,'tokenNum':tokenNum},
        async: false,
        success: function(data) {
            var data = $.parseJSON(data);$.unblockUI();
            if(data.error == 1) {
                alert(data.message);
            } else {
                window.open(SITE_URL+data.fileName, '_blank');
            }
        },
        error: function(e) {
        }
    });
}
//function to delete doc parts
function deleteDocPart(val) {
    if(val != '') {
        var checkedItemsAsString = $('[name="checkDocPart"]:checked').map(function() { return $(this).val().toString(); } ).get().join(",");
        if(checkedItemsAsString != '') {
            if ($('[name="checkDocPart"]').filter(':not(:checked)').length == 0) {
                alert("Atleast one choeckbox should be unchecked");
            } else {
                blockUi();
                var url = SITE_URL+'document/deletedocpart';
                $.ajax({
                    url: url,
                    type: 'Post',
                    data: {'ids':checkedItemsAsString},
                    async: false,
                    success: function(data) {
                        var data = $.parseJSON(data);$.unblockUI();
                        if(data.error == 1) {
                            alert(data.message);
                        } else {
                            alert(data.message); 
                            var docId = $("#docid").val();
                            var urldoc = SITE_URL+'document/document?id='+docId;
                            window.location = urldoc;
                        }
                    },
                    error: function(e) {
                    }
                });
            }
        } else {
            alert("No checkbox is seletcted to delete document parts");
        }
    }
}
//function to make document publish
function publishVersion(docId) {
    blockUi();    
    var url = SITE_URL+'document/publishdocument';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'docId':docId},
        async: false,
        success: function(data) {
            var data = $.parseJSON(data);$.unblockUI();
            if(data.error == 1) {
                alert(data.message);
            } else {
                var urldoc = SITE_URL+'document/documentlist';
                window.location = urldoc;
            }
        },
        error: function(e) {
        }
    });
}
//function to move up and down doc part
function moveDocPart(docPartId,type) {
    blockUi();
    var url = SITE_URL+'document/changedocpartorder';
    $.ajax({
        url: url,
        type: 'Post',
        data: {'docPartId':docPartId,'type':type},
        async: false,
        success: function(data) {
            var data = $.parseJSON(data);$.unblockUI();
            if(data.error == 1) {
                alert(data.message);
            } else {
                alert(data.message);
                var docId = $("#docid").val();
                var urldoc = SITE_URL+'document/document?id='+docId;
                window.location = urldoc;
            }
        },
        error: function(e) {
        }
    });
}
//function reponsible for block ui
function blockUi() {
    $.blockUI({ message: 'Please wait',css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 0.5, 
            color: '#fff' 
        }});
}
