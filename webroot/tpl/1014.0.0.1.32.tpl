<html>
<head>
<link rel='stylesheet' href='/home/dev2bigideas/public_html/evolaw/webroot/document_templates/css/workflow_template_id_1.css' type='text/css'>
</head>
<body>

{if 1}# FORBEARANCE AGREEMENT

{/if}
{if 1}<!-- BIG BLUE LINE -->ok

{/if}
{if $facaotherdebtors == "No"}

This Forbearance Agreement ("Agreement") is made as of {$facadate|full_text_date} by and between {$facacreditorname} ("{$varcreditor}") and {$facadebtorname} ("{$vardebtor}").  {$varcreditor} and {$vardebtor} may collectively be referred to in this Agreement as the "Parties," and individually as a "Party."


{/if}
{if $facaotherdebtors == "Yes"}

This Forbearance Agreement ("Agreement") is made as of {$facadate|full_text_date} by and between {$facacreditorname} ("{$varcreditor}") on the one hand, and {$vardebtornames} (collectively, "{$vardebtor}") on the other hand.   {$varcreditor} and {$vardebtor} may collectively be referred to in this Agreement as the "Parties," and individually as a "Party."


{/if}
{if 1}## RECITALS

{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "1" && $facalendinterest == "Yes" && $facaignoreinterest == "No" && $facaotherdebtors == "No"}A. In or around {$facadocdate1|date_month_of_year}, {$varcreditor} entered into a written agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of ${$facaloanamt1} from {$varcreditor}.  {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%.  To date, {$vardebtor} owes {$varcreditor} ${$facainterestcalc} in unpaid interest.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "1" && $facalendinterest == "Yes" && $facaignoreinterest == "No" && $facaotherdebtors == "Yes"}A. In or around {$facadocdate1|date_month_of_year}, {$varcreditor} entered into a written agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of ${$facaloanamt1} from {$varcreditor}.  {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%.  To date, {$vardebtor} owe {$varcreditor} ${$facainterestcalc} in unpaid interest.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "2" && $facalendinterest == "Yes" && $facaignoreinterest == "No"}A. In or around {$facadate2ormore1st|date_month_of_year}, {$varcreditor} entered into a written agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of ${$facaloanamt1} from {$varcreditor}.  Thereafter, in or around {$facadate1lst|date_month_of_year}, {$varcreditor} and {$vardebtor} entered into a second written loan agreement for an additional sum of ${$faca2ndloanamt}, bringing the total principal sum borrowed by {$vardebtor} to ${$facaloanamt2total}.  {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%, which to date, totals ${$facainterestcalc}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "3" or "4" or "5" or "6" or "7" or "8" or "9" or "10+" && $facalendinterest == "Yes" && $facaignoreinterest == "No"}A.  In or around {$facadate2ormore1st|date_month_of_year}, {$varcreditor} entered into the first of several written agreements with  {$vardebtor} in which {$vardebtor} borrowed the sum of  ${$faca1stloanamt} from {$varcreditor}.  Thereafter, {$varcreditor} entered into {$varnumofwritings|subtract_1} additional written loan agreements with {$vardebtor}, bringing the total principal sum borrowed by {$vardebtor} to ${$facaloanamt2total}. {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%, which to date, totals ${$facainterestcalc}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "An oral agreement" && $facaoralaskhowmany == "Yes" && $facalendinterest == "Yes" && $facaignoreinterest == "No"}A. In or around {$facaoraldate1|date_month_of_year}, {$varcreditor} entered into an oral agreement with {$vardebtor} in which over time, {$vardebtor} borrowed the total sum of ${$facaoralloanamt} from {$varcreditor}.  {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%, which to date, totals ${$facainterestcalc}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "An oral agreement" && $facaoralaskhowmany == "No" && $facalendinterest == "Yes" && $facaignoreinterest == "No"}A. In or around {$facaoraldate1|date_month_of_year}, {$varcreditor} entered into an oral agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of ${$facaoralloanamt} from {$varcreditor}.  {$vardebtor} also agreed to pay interest on the unpaid portion of the principal balance at the {$varinterest} {$facainterestamt}%, which to date, totals ${$facainterestcalc}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "1" && $facalendinterest == "No" or $facaignoreinterest == "Yes"}A. In or around {$facadocdate1|date_month_of_year}, {$varcreditor} entered into a written agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of {$facaloanamt1} from {$varcreditor}.  
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "2" && $facalendinterest == "No" or $facaignoreinterest == "Yes"}A. In or around {$facadate2ormore1st|date_month_of_year}, {$varcreditor} entered into a written agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of {$facaloanamt1} from {$varcreditor}.  Thereafter, in or around {$facadate1lst|date_month_of_year}, {$varcreditor} and {$vardebtor} entered into a second written loan agreement for an additional sum of ${$faca2ndloanamt}, bringing the total principal sum borrowed by {$vardebtor} to ${$facaloanamt2total}.  
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "3" or "4" or "5" or "6" or "7" or "8" or "9" or "10+" && $facalendinterest == "No" or $facaignoreinterest == "Yes"}A. In or around {$facadate2ormore1st|date_month_of_year}, {$varcreditor} entered into the first of several written agreements with  {$vardebtor} in which {$vardebtor} borrowed the sum of  ${$faca1stloanamt} from {$varcreditor}.  Thereafter, {$varcreditor} entered into {$varnumofwritings|subtract_1} additional written loan agreements with {$vardebtor}, bringing the total principal sum borrowed by {$vardebtor} to ${$facaloanamt2total}.  
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "An oral agreement" && $facaoralaskhowmany == "Yes" && $facalendinterest == "No" or $facaignoreinterest == "Yes"}A. In or around {$facaoraldate1|date_month_of_year}, {$varcreditor} entered into an oral agreement with {$vardebtor} in which over time, {$vardebtor} borrowed the total sum of ${$facaoralloanamt} from {$varcreditor}.  
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "An oral agreement" && $facaoralaskhowmany == "No" && $facalendinterest == "No" or $facaignoreinterest == "Yes"}A. In or around {$facaoraldate1|date_month_of_year}, {$varcreditor} entered into an oral agreement with {$vardebtor} in which {$vardebtor} borrowed the sum of ${$facaoralloanamt} from {$varcreditor}.  
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facasvccontract == "Yes" && $facawhensvcs != $facasvcpmtdue}A. In or around {$facawhensvcs|date_month_of_year}, {$varcreditor} and {$vardebtor} entered into a written agreement in which {$vardebtor} hired {$varcreditor} to perform various {$varservices} related services (the “Services”), in return for which {$vardebtor} agreed to pay {$varcreditor} the sum of ${$facasvchowmuch}.  The agreement obligated {$vardebtor} to tender payment to {$varcreditor} in  {$facasvcpmtdue|date_month_of_year}.  The Parties acknowledge and agree that {$varcreditor} competently performed the agreed upon Services.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facasvccontract == "No" && $facawhensvcs != $facasvcpmtdue}A. In or around {$facawhensvcs|date_month_of_year}, {$vardebtor} hired {$varcreditor} to perform various {$varservices} related services (the “Services”), in return for which {$vardebtor} agreed to pay {$varcreditor} the sum of ${$facasvchowmuch}.  {$vardebtor} agreed to tender payment to {$varcreditor} in {$facasvcpmtdue|date_month_of_year}.  The Parties acknowledge and agree {$varcreditor} competently performed the agreed upon Services.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facasvccontract == "Yes" or "No" && $facawhensvcs == $facasvcpmtdue}A. In or around {$facawhensvcs|date_month_of_year}, {$vardebtor} hired {$varcreditor} to perform various {$varservices} related services (the “Services”), in return for which {$vardebtor} agreed to pay {$varcreditor} the sum of ${$facasvchowmuch}.  {$vardebtor} agreed to tender payment to {$varcreditor} later that month.  The Parties acknowledge and agree {$varcreditor} competently performed the agreed upon Services.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "1"}A. In or around {$facaloanamt1dead|date_month_of_year}, {$vardebtor} breached the written agreement by failing to tender the agreed upon payment to {$varcreditor}.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facasvccontract == "Yes" or "No" && $facawhensvcs == $facasvcpmtdue}A. In or around {$facaloanamt1dead|date_month_of_year}, {$vardebtor} breached the written agreement by failing to tender the agreed upon payment to {$varcreditor}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "2" or "3" or "4" or "5" or "6" or "7" or "8" or "9" or "10+"}B.   In or around {$facaloanamt1dead|date_month_of_year}, {$vardebtor} breached the written agreement by failing to tender the agreed upon payment to {$varcreditor}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "One or more written documents" && $facahowmanydocs == "2" or "3" or "4" or "5" or "6" or "7" or "8" or "9" or "10+"}B. In or around {$facaloanamt2dead|date_month_of_year}, {$vardebtor} breached the written agreements by failing to tender payment(s) to {$varcreditor} as required under the written agreements.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facamoneyevidence == "An oral agreement" }B. In or around {$facaoraldate1|date_month_of_year}, {$vardebtor} breached the oral agreement by failing to timely tender the agreed upon payment(s) to {$varcreditor}.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes"}B. {$vardebtor} breached the agreement with {$varcreditor} by failing to tender the agreed upon payment when it became due in {$facasvcpmtdue|date_month_of_year}.    
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facaotherdebtors == "No"}C. Notwithstanding {$varcreditor}’s demand that {$vardebtor} abide by {$vardebtor}’s obligations under the loan agreement(s) with {$varcreditor}, {$vardebtor} has thus far failed and refused to tender the agreed upon sum to {$varcreditor}.
{/if}
{if $facalendmoney == "Yes" && $facaservices == "No" && $facaotherdebtors == "Yes"}C. Notwithstanding {$varcreditor}’s demand that {$vardebtor} abide by their obligations under the loan agreement(s) with {$varcreditor}, {$vardebtor} have thus far failed and refused to tender the agreed upon sum to {$varcreditor}.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facaotherdebtors == "No"}C. Notwithstanding {$varcreditor}’s demand that {$vardebtor} abide by {$vardebtor}’s obligation to pay {$varcreditor} for having rendered the Services, {$vardebtor} has thus far failed and refused to tender the agreed upon sum.
{/if}
{if $facalendmoney == "No" && $facaservices == "Yes" && $facaotherdebtors == "Yes"}C. Notwithstanding {$varcreditor}’s demand that {$vardebtor} abide by their obligations to pay {$varcreditor} for having rendered the Services, {$vardebtor} have thus far failed and refused to tender the agreed upon sum.
{/if}
{if $facaotherdebtors == "No"}D. As of today, {$vardebtor} owes {$varcreditor} the sum of ${$vargrandtotal}.    
{/if}
{if $facaotherdebtors == "Yes"}D. As of today, {$vardebtor} owe {$varcreditor} the sum of ${$vargrandtotal}.
{/if}
{if $facaotherdebtors == "No"}E. {$varcreditor} has elected to forebear the filing of a lawsuit in exchange for {$vardebtor}’s agreement to be bound by the provisions set forth in this Agreement, which shall expressly supersede any previous oral or written understandings or agreements between the Parties.
{/if}
{if $facaotherdebtors == "Yes"}E. {$varcreditor} has elected to forebear the filing of a lawsuit in exchange for {$vardebtor}’ agreement to be bound by the provisions set forth in this Agreement, which shall expressly supersede any previous oral or written understandings or agreements between the Parties.
{/if}
{if 1}

NOW, THEREFORE, in consideration of the promises and mutual representations, warranties, and covenants which are to be made and performed by the Parties, the Parties agree as follows:


{/if}
{if 1}## Agreement
{/if}
{if 1}### Recitals
{/if}
{if 1}

The Parties incorporate the above-referenced Recitals into this Agreement by this reference.


{/if}
{if 1}### Payments
{/if}
{if $facalumporplan == "One lump sum" && $facainterest > 0}

On or before {$facalumpdue|full_text_date}, {$vardebtor} shall tender to {$varcreditor} the principal sum of ${$vargrandtotal}, together with interest due at the annual rate of {$facainterest}%, or the maximum rate permitted by law, whichever is less, for a total sum of ${calculate_amount_with_simple_interest grandtoal=$vargrandtotal intrate=$facainterest duepayment1=$faca1stdatedue duepaymentlast=$facalumpdue}.  Payment shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "One lump sum" && $facainterest == 0}

On or before {$facalumpdue|full_text_date}, {$vardebtor} shall tender to {$varcreditor} the principal sum of ${$vargrandtotal}.  Payment shall be made {$varlumphowpaid}. Payment shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "Payment plan" && $facainterest > 0 && $facaotherdebtors == "No"}

{$vardebtor} shall tender to {$varcreditor}, in {$facapmtplandue} installments, the principal sum of ${$vargrandtotal}, together with interest at the annual rate of {$facainterest}%, or the maximum rate permitted by law, whichever is less, pursuant to the Payment Schedule attached as Exhibit "A," which is incorporated into this Agreement by this reference.  {$vardebtor}’s first payment under this Forbearance Agreement shall be due on {$faca1stdatedue}, and each subsequent payment shall be tendered on {$varfreq|strtolower} basis, also as specified in Exhibit "A."  Payments shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "Payment plan" && $facainterest >0 && $facaotherdebtors == "Yes"}

{$vardebtor} shall tender to {$varcreditor}, in {$facapmtplandue} installments, the principal sum of ${$vargrandtotal}, together with interest at the annual rate of {$facainterest}%, or the maximum rate permitted by law, whichever is less, pursuant to the Payment Schedule attached as Exhibit "A," which is incorporated into this Agreement by this reference.  {$vardebtor}’ first payment under this Forbearance Agreement shall be due on {$faca1stdatedue}, and each subsequent payment shall be tendered on {$varfreq|strtolower} basis, also as specified in Exhibit "A."  Payments shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "Payment plan" && $facainterest == 0 && $facaotherdebtors == "No"}

{$vardebtor} shall tender to {$varcreditor}, in {$facapmtplandue} installments, the principal sum of ${$vargrandtotal} pursuant to the Payment Schedule attached as Exhibit "A," which is incorporated into this Agreement by this reference.  {$vardebtor}’s first payment under this Forbearance Agreement shall be due on {$faca1stdatedue}, and each subsequent payment shall be tendered on {$varfreq|strtolower} basis, also as specified in Exhibit "A."  Payments shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "Payment plan" && $facainterest == 0 && $facaotherdebtors == "Yes"}

{$vardebtor} shall tender to {$varcreditor}, in {$facapmtplandue} installments, the principal sum of ${$vargrandtotal} pursuant to the Payment Schedule attached as Exhibit "A," which is incorporated into this Agreement by this reference.  {$vardebtor}’ first payment under this Forbearance Agreement shall be due on {$faca1stdatedue}, and each subsequent payment shall be tendered on {$varfreq|strtolower} basis, also as specified in Exhibit "A."  Payments shall be made {$varlumphowpaid}.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes"}### Late Charge
{/if}
{if 1}

There shall be a late charge equal to {$varlatefee} on all payments *received* {$facaplanlatefeecure} or more days after the due date.  It is {$vardebtor}’s responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes" && $facaplanlatefeecure >= 1 && $facaplanlatefeecure <= 20 && $facaplanlatefeeamt != "A specified dollar amount" && $facaotherdebtors == "No"}

There shall be a late charge equal to {$varlatefee} on all payments *received* {$facaplanlatefeecure} or more days after the due date.  It is {$vardebtor}’ responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes" && $facaplanlatefeecure >= 1 && $facaplanlatefeecure <= 20 && $facaplanlatefeeamt != "A specified dollar amount" && $facaotherdebtors == "Yes"}

A late charge of {$varlatedollaramt} shall be due on each payment not *received* {$facaplanlatefeecure} or more days after the due date.  It is {$vardebtor}’s responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes" && $facaplanlatefeeamt == "A specified dollar amount" && $facaotherdebtors == "Yes"}

A late charge of {$varlatedollaramt} shall be due on each payment not *received* {$facaplanlatefeecure} or more days after the due date.  It is {$vardebtor}’ responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes" && $facaplanlatefeecure == "No grace period" && $facaplanlatefeeamt != "A specified dollar amount" && $facaotherdebtors == "No"}

A late charge of {$varlatefee} shall be due on each payment not *received* by {$varcreditor} on or before the due date.  It is {$vardebtor}’s responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facalumporplan == "Payment plan" && $facaplanlatefee == "Yes" && $facaplanlatefeecure == "No grace period" && $facaplanlatefeeamt != "A specified dollar amount" && $facaotherdebtors == "Yes"}

A late charge of {$varlatefee} shall be due on each payment not *received* by {$varcreditor} on or before the due date.  It is {$vardebtor}’ responsibility to ensure that all payments are *received* by {$varcreditor} *on or before* the deadline, or such payments will incur the late charge.


{/if}
{if $facaeventsofdefault == "Yes" && $facaotherdebtors == "No"}### Events of Default
{/if}
{if $facaeventsofdefault == "Yes" && $facaotherdebtors == "No"}

In addition to {$vardebtor}’s failure to make timely payments required under this Agreement, any one of the following shall also constitute events of default, thus accelerating the debt and rendering the entire sum of principal and interest due and payable immediately: (i) {$vardebtor}’s insolvency; (ii) {$vardebtor}’s filing of a voluntary bankruptcy (or the involuntary filing of a bankruptcy against {$vardebtor} which is not dismissed within 60 days of its filing); or (iii) {$vardebtor}’s making of a general assignment for the benefit of creditors.  


{/if}
{if $facaeventsofdefault == "Yes" && $facaotherdebtors == "Yes"}### Events of Default
{/if}
{if $facaeventsofdefault == "Yes" && $facaotherdebtors == "Yes"}

In addition to {$vardebtor}’ failure to make timely payments required under this Agreement, any one of the following shall also constitute events of default, thus accelerating the debt and rendering the entire sum of principal and interest due and payable immediately: (i) {$vardebtor}’ insolvency; (ii) {$vardebtor}’ filing of a voluntary bankruptcy (or the involuntary filing of a bankruptcy against {$vardebtor} which is not dismissed within 60 days of its filing); or (iii) {$vardebtor}’ making of a general assignment for the benefit of creditors.


{/if}
{if 1}#### General Provisions
{/if}
{if 1}In addition to the specific provisions set forth above, the Parties also agree to the following:
{/if}
{if $facaeventsofdefault == "No" && $facaotherdebtors == "No"}Acceleration
{/if}
{if $facaeventsofdefault == "No" && $facaotherdebtors == "No"}

If {$vardebtor} fails to timely make one or more payments of principal or interest due under this Agreement, and if {$vardebtor} fails to cure that default within {$varlatefeeno} days of the payment(s) due date, {$varcreditor} may, in {$varcreditor}’s sole discretion, accelerate the debt such that the entire sum of principal and interest then owing shall become immediately due and payable.


{/if}
{if $facaeventsofdefault == "No" && $facaotherdebtors == "Yes"}Acceleration
{/if}
{if $facaeventsofdefault == "No" && $facaotherdebtors == "Yes"}

If {$vardebtor} fail to timely make one or more payments of principal or interest due under this Agreement, and if {$vardebtor} fail to cure that default within {$varlatefeeno} days of the payment(s) due date, {$varcreditor} may, in {$varcreditor}’s sole discretion, accelerate the debt such that the entire sum of principal and interest then owing shall become immediately due and payable.


{/if}
{if 1}Notices
{/if}
{if 1}

All notices required under this Agreement shall be in writing and shall be delivered either via: (i) personal delivery; or (ii) Federal Express, United Parcel Service, or certified mail, return receipt requested.  Notices shall be delivered to the following addresses:


{/if}
{if 1}If to {$varcreditor}
{/if}
{if 1}

{$facanoticecred}


{/if}
{if $facaotherdebtors == "No"}If to {$vardebtor}
{/if}
{if $facaotherdebtors == "No"}

{$facanoticedebt}


{/if}
{if $facaotherdebtors == "Yes"}If to {$vardebtor}
{/if}
{if $facaotherdebtors == "No"}

{$facadebtoraddress}


{/if}
{if $facaotherdebtors == "Yes"}

{$facadebtorname}: {$facadebtoraddress}


{/if}
{if $facaotherdebtors == "Yes"}

{$facanameotherdebt1}: {$facaaddotherdebt1}


{/if}
{if $facaanyothers1 == "Yes"}

{$facanameotherdebt2}: {$facaaddotherdebt2}


{/if}
{if $facaanyothers2 == "Yes"}

{$facanameotherdebt3}: {$facaaddotherdebt3}


{/if}
{if $facaanyothers3 == "Yes"}

{$facanameotherdebt4}: {$facaaddotherdebt4}


{/if}
{if 1}#### No Breach or Violation
{/if}
{if 1}

The Parties represent that their execution and delivery of this Agreement and/or performance of their respective obligations under this Agreement do not: (i) conflict with, violate, result in a breach, termination or cancellation of, or constitute a default in or under, any other agreement to which either is a party; (ii) violate any order, writ, injunction, decree, judgment, or ruling of any court or governmental authority; or (iii) violate any applicable law.


{/if}
{if 1}#### Requisite Authority.
{/if}
{if 1}

If any of the Parties are corporations, limited liability companies, and/or limited liability partnerships, the individuals signing this Agreement on behalf of such entity(ies) represent and warrant that they have all requisite corporate power and authority to enter into this Agreement.  If, after signing this Agreement, facts arise demonstrating that any such individuals lacked such corporate power and authority, such individuals shall be personally liable for the debt set forth in this Agreement.


{/if}
{if 1}#### Entire Agreement / Modifications in Writing.
{/if}
{if 1}

This Agreement constitutes the entire agreement between the Parties concerning the debt referenced above, and supersedes all previous agreements, either written or oral, concerning this Agreement’s subject matter.  This Agreement may only be amended or modified by a written document signed by the Parties.


{/if}
</body>
</html>