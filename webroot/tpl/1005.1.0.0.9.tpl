<html>
<head>
<link rel='stylesheet' href='/home/dev2bigideas/public_html/evolaw/webroot/document_templates/css/workflow_template_id_1.css' type='text/css'>
</head>
<body>
{if 1}<h1>VEHICLE USE POLICY</h1>

{/if}
{if 1}<h2>INTRODUCTION AND PURPOSE</h2>

{/if}
{if 1}<p>Once You sign the Acknowledgment of Receipt at the end of this Policy, it shall be incorporated by reference into Your At-Will Employment Agreement, which You may have already signed or will soon sign.</p>

{/if}
{If $gfullnameco != "" }<p>You shall operate the Vehicles in a safe and lawful manner at all times.  This means that You must always: (i) maintain a safe speed; (ii) abide by all traffic lights and signs; (iii) wear Your seatbelt, and require passengers, if any, to wear their seatbelts; (iv) drive carefully if required to drive in hazardous conditions; and/or (v) otherwise obey all applicable traffic and safety laws.</p>

{/if}
</body>
</html>