<html>
<head>
<link rel='stylesheet' href='/home/dev2bigideas/public_html/evolaw/webroot/document_templates/css/workflow_template_id_1.css' type='text/css'>
</head>
<body>
{if 1}<h1>VEHICLE USE POLICY</h1>

{/if}
{if 1}<h2>INTRODUCTION AND PURPOSE</h2>

{/if}
{if 1}<p>In addition to the foregoing, at all times You must:  (i) refrain from discussing 
the accident with anyone at the scene except the police; (ii) never accept 
responsibility for the accident (it is sufficient to just provide the facts to the 
police when questioned); and/or (iii) avoid arguing with anyone about the facts 
surrounding the accident.</p>

{/if}
{if 1}<p>In addition, You must, within 1 business day of Your arrest or citation for or 
under any statute related to driving under the influence of alcohol or drugs, or 
driving while impaired/intoxicated, provide {$gfullnameco} with 
written notification of the arrest or citation.</p>

{/if}
</body>
</html>