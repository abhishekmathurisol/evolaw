<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.fulltextdate.php
 * Type:     modifier
 * Name:     capitalize
 * Purpose:  capitalize words in the string
 * -------------------------------------------------------------
 */
function smarty_modifier_subtract_1($number)
{
	return $number--;
}
?>