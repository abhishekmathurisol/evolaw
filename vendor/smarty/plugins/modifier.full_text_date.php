<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.fulltextdate.php
 * Type:     modifier
 * Name:     capitalize
 * Purpose:  capitalize words in the string
 * -------------------------------------------------------------
 */
function smarty_modifier_full_text_date($string)
{
	return  date("F j, Y", strtotime($string));
}
?>