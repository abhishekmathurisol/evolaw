<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {calculate_amount_with_simple_interest} function plugin
 * Type:     function<br>
 * Name:     html_table<br>
 * Date:     Feb 17, 2003<br>
 * Purpose:  make an html table from an array of data<br>
 * Params:
 * <pre>
 * - blah       - blah
 * </pre>
 * Examples:
 * <pre>
 * {calculate_amount_with_simple_interest grandtoal=$1 intrate=$2 duepayment1=$3 duepaymentlast=$4}
 * </pre>
 *
 * @author   Michael Craig <mcraig@bytelaunch.com>
 * @version  0.1
 *
 * @param array $params parameters
 *
 * @return string
 */
function smarty_function_calculate_amount_with_simple_interest($params)
{
      
	$output = "1001.37";

    return $output;
}
