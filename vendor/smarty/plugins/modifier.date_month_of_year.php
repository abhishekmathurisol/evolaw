<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.fulltextdate.php
 * Type:     modifier
 * Name:     capitalize
 * Purpose:  capitalize words in the string
 * -------------------------------------------------------------
 */
function smarty_modifier_date_month_of_year($string)
{
	return  date("F of Y", strtotime($string));
}
?>